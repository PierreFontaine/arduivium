# arduivium

A repo that include all sources to load a Trivium architecture on the MKRVIDOR 4000

## ReverseByte_V2

Utility from *systemes-embarques.fr* [https://systemes-embarques.fr/wp/telechargement/](https://systemes-embarques.fr/wp/telechargement/)

## VidorFPGA

Project directory for Quartus

## VidorBoot

Project directory for Arduino IDE
