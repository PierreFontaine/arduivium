library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;

entity top_trivium is
    port (
        clk, rst	    : in std_logic;
        start_crypt     : in std_logic;
        out_z, gen_key, bit_ciphered, clk_prob    : out std_logic
    ) ;
end top_trivium ;

architecture top_trivium_arch of top_trivium is
    
    -- Ajout du composant trivium
    
    component trivium is
        port (
            clk, rst : in std_logic;
            key, iv         : in std_logic_vector(79 downto 0); 
            start_crypt     : in std_logic;
            bit_to_cipher   : in std_logic;
            bit_ciphered, gen_key    : out std_logic;
            out_z : out std_logic
        ) ;
    end component;
    
    component key_iv_rom is
        Port (
            clk : in std_logic;
            iv, key : out std_logic_vector(79 downto 0)
        );
    end component;
    
    signal key_feed : std_logic_vector (79 downto 0);
    signal iv_feed : std_logic_vector (79 downto 0);
    signal bit_to_cipher : std_logic;
    -- signal clk_div : std_logic_vector(100 downto 1) := ((100 downto 2 =>'0') & '1');
    signal in_z_feed, out_z_feed : std_logic;
    signal s_clk_1hz: std_logic := '0';
  
  -- declaration constant
  constant prescaler: integer := 100;
begin
    
    bit_to_cipher <= '0';
    
    prof_divider : process(clk)
    variable count : integer range 0 to 500-1 := 0;
    begin
        if rising_edge(clk) then
            if (count = prescaler) then
                count := 0;
                s_clk_1hz <= not s_clk_1hz;
            else
                count := count + 1;
            end if;
        end if;   
    end process;  
    
    out_z <= out_z_feed;
    clk_prob <= s_clk_1hz;
    
    rom : key_iv_rom port map (s_clk_1hz, iv_feed, key_feed);
    -- device to monitor
    dtm : trivium port map (s_clk_1hz, rst, key_feed, iv_feed, start_crypt, bit_to_cipher, bit_ciphered, gen_key, out_z_feed);
    
    
end architecture;