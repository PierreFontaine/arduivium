library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity key_iv_rom is
  Port (
    clk : in std_logic;
    iv, key : out std_logic_vector(79 downto 0)
  );
end key_iv_rom;

architecture Behavioral of key_iv_rom is    
    attribute keep:string;
    attribute keep of key : signal is "true";
    attribute keep of iv : signal is "true";
begin
    process (clk)
        constant key_reg : std_logic_vector(79 downto 0) := X"00000000000000000000";
        constant iv_reg  : std_logic_vector(79 downto 0) := X"00000000000000000000";
        
    begin
        if (rising_edge(clk)) then
            key <= key_reg;
            iv  <= iv_reg;
        end if;
    end process;
end Behavioral;
