--------------------------------------------------------------
--! @file
--! @brief Machine a état permettant de controller le 
--! datapath. Nous y gérons 3 états qui sont : init,
--! warmup et generate_key.
--! @author Fontaine Pierre
--------------------------------------------------------------

library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;
    use ieee.std_logic_signed.all;

--! @param clk horloge du système
--! @param rst signal permettant un reset asynchrone
--! @param start signal permettant d'initialiser le process
--! @param z flux d'entrée pour le keystream
--! @param bit_to_cipher flux d'entrée pour le plaintext
--! @param bit_ciphered flux de sortie pour le ciphertext
--! @param load_init signal de sortie pour controller les 3
--! différents registres en phase d'initialisation
--! @param load signal de sortie pour controller l'état
--! des 3 différents registres pour l'écriture
--! @param gen_key signal pour signaler le début de la génération
--! de la clé
entity uc_trivium is
    port (
        clk, rst            : in std_logic;
        start               : in std_logic;
        z                   : in std_logic;
        bit_to_cipher       : in std_logic;
        bit_ciphered	    : out std_logic;
        load_init, load, gen_key     : out std_logic
    ) ;
end uc_trivium ; 

architecture uc_trivium_arch of uc_trivium is

    type states is ( init, warmup, generatekey );
    signal pr_state : states := init;
    signal rst_counter : std_logic;
    
begin

    new_fsm : process (clk, rst)
    variable counter : integer := 0;
    begin
        --! Gestion du reset asynchrone
        if (rst = '1') then
            pr_state <= init;
            bit_ciphered <= '0';
            gen_key <= '0';
            load <= '0';
            load_init <= '0';
            counter := 0;
        else
            if rising_edge(clk) then
                case pr_state is
                    when init =>
                        bit_ciphered <= '0';
                        gen_key <= '0';
                        counter := 0;
                        
                        if (start = '1') then
                            load <= '1';
                            load_init <= '1';
                            pr_state <= warmup;
                        else
                            load <= '0';
                            load_init <= '0';
                        end if;
                    when warmup =>
                        counter := counter + 1;
                        load_init <= '0';
                        load <= '1';
                        bit_ciphered <= '0';
                        gen_key <= '0';
                        
                        if counter > (4*288) - 1 then
                            pr_state <= generatekey;
                            counter := 0;
                        else
                            NULL;
                        end if;
                        
                    when generatekey =>
                        load <= '1';
                        load_init <= '0';
                        gen_key <= '1';
                        bit_ciphered <= bit_to_cipher xor z;
                    when Others => Null;
                end case;
                
            end if;
       end if;
    end process;
end architecture;