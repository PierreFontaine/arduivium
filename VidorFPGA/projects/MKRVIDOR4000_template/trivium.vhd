--------------------------------------------------------------
--! @file
--! @brief element principal gérant les connexions 
--! inter composants, ainsi que les I/O et horloge.
--! @author Fontaine Pierre
--------------------------------------------------------------
library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;

--! @param clk horloge du système
--! @param rst signal pour un reset asynchrone
--! @param key flux d'entrée pour la clé
--! @param iv flux d'entrée pour l'iv
--! @param start_crypt signal pour démarrer la séquence init
--! warmup et generate_key
--! @param bit_to_cipher flux d'entrée pour le plaintext
--! @param bit_ciphered flux de sortie pour le ciphertext
--! @param gen_key signal pour signaler le début de la 
--! génération de la clé
--! @param out_z flux de sortie pour le keystream
entity trivium is
    port (
        clk, rst	    : in std_logic;
        key, iv         : in std_logic_vector(79 downto 0); 
        start_crypt     : in std_logic;
        bit_to_cipher   : in std_logic;
        bit_ciphered, gen_key    : out std_logic;
        out_z : out std_logic
    ) ;
end trivium ; 

architecture trivium_arch of trivium is

    component reg_111_b is
        generic(s : integer);
        port (
            clk, init, rst, load    : in std_logic;
            x                       : in std_logic_vector(s -1 downto 0);
            x_out                   : out std_logic_vector(s -1 downto 0)
        );
    end component;

    component reg_x_b is
        generic (s : integer := 100);
        port (
            clk, init, rst, load    : in std_logic;
            x                       : in std_logic_vector(s -1 downto 0);
            x_init                  : in std_logic_vector(80 -1 downto 0);
            x_out                   : out std_logic_vector(s -1 downto 0)
        );
    end component;

    component T_ALU is
        port (
            --clk : in std_logic;
            in_reg_1    : in std_logic_vector(93 - 1 downto 0);
            in_reg_2    : in std_logic_vector(84 - 1 downto 0);
            in_reg_3    : in std_logic_vector(111 - 1 downto 0);
            out_z       : out std_logic;
            out_reg_1   : out std_logic_vector(93 - 1 downto 0);
            out_reg_2   : out std_logic_vector(84 - 1 downto 0);
            out_reg_3   : out std_logic_vector(111 - 1 downto 0)
        ) ;
    end component;

    component uc_trivium is
        port (
            clk, rst            : in std_logic;
            start               : in std_logic;
            z                   : in std_logic;
            bit_to_cipher       : in std_logic;
            bit_ciphered	    : out std_logic;
            load_init, load, gen_key  : out std_logic
        );
    end component;


    signal load_init_feed, load_feed	            : std_logic;
    signal x_reg_key_in_feed, x_reg_key_out_feed    : std_logic_vector(92 downto 0);
    signal x_reg_iv_in_feed, x_reg_iv_out_feed      : std_logic_vector(83 downto 0);
    signal x_reg_111_in_feed, x_reg_111_out_feed    : std_logic_vector(110 downto 0);
    signal out_z_feed                               : std_logic;
  
begin

    out_z <= out_z_feed;

    reg_key :   reg_x_b generic map (s => 93)
    port map (clk, load_init_feed, rst, load_feed, x_reg_key_in_feed, key, x_reg_key_out_feed);

    reg_iv  :   reg_x_b generic map (s => 84)
    port map (clk, load_init_feed, rst, load_feed, x_reg_iv_in_feed, iv, x_reg_iv_out_feed);

    reg     :   reg_111_b generic map (s => 111) 
    port map (clk, load_init_feed, rst, load_feed, x_reg_111_in_feed, x_reg_111_out_feed);

    talu    :   T_ALU port map (x_reg_key_out_feed, x_reg_iv_out_feed, x_reg_111_out_feed,out_z_feed,x_reg_key_in_feed,x_reg_iv_in_feed,x_reg_111_in_feed);
    uc_triv :   uc_trivium port map (clk, rst, start_crypt, out_z_feed, bit_to_cipher, bit_ciphered, load_init_feed, load_feed, gen_key);

end architecture ;