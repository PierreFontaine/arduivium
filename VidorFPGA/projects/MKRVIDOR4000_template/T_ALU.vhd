--------------------------------------------------------------
--! @file
--! @brief LFSR opérateur sur 3 registres
--! @author Fontaine Pierre
--------------------------------------------------------------


--! @brief Utilisation de la librairie standard
library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;

--! @param in_reg_1 flux d'entrée pour le registre de clé
--! @param in_reg_2 flux d'entrée pour le registre de vecteur d'initialisation
--! @param in_reg_3 flux d'entrée pour le registre restant
--! @param out_z flux de sortie pour le keystream
--! @param in_reg_1 flux de sortie pour le registre de clé
--! @param in_reg_2 flux de sortie pour le registre de vecteur d'initialisation
--! @param in_reg_3 flux de sortie pour le registre restant
entity t_alu is
  port (
    in_reg_1    : in std_logic_vector(93 - 1 downto 0); 
    in_reg_2    : in std_logic_vector(84 - 1 downto 0); 
    in_reg_3    : in std_logic_vector(111 - 1 downto 0);
    out_z       : out std_logic;
    out_reg_1   : out std_logic_vector(93 - 1 downto 0);
    out_reg_2   : out std_logic_vector(84 - 1 downto 0);
    out_reg_3   : out std_logic_vector(111 - 1 downto 0)
  ) ;
end t_alu ; 

--! @brief Architecture ayant pour role d'opérer sur des bits 
--! précis de chacun des registres
architecture arch of t_alu is
    signal t1_1, t2_1, t3_1 : std_logic;
    signal t1, t2, t3 : std_logic;
    signal out_reg_1_tmp   : std_logic_vector(93 - 1 downto 0);
    signal out_reg_2_tmp   : std_logic_vector(84 - 1 downto 0);
    signal out_reg_3_tmp   : std_logic_vector(111 - 1 downto 0);
    signal z_loc : std_logic;
begin
    
    t1_1 <= in_reg_1(65) xor in_reg_1(92); -- verified
    t2_1 <= in_reg_2(68) xor in_reg_2(83); -- verified
    t3_1 <= in_reg_3(65) xor in_reg_3(110); -- verified

    t1 <= t1_1 xor (in_reg_1(90) and in_reg_1(91)) xor in_reg_2(77); -- verified
    t2 <= t2_1 xor (in_reg_2(81) and in_reg_2(82)) xor in_reg_3(86); -- verified
    t3 <= t3_1 xor (in_reg_3(108) and in_reg_3(109)) xor in_reg_1(68); -- verified

    out_z <= t1_1 xor (t2_1 xor t3_1);

    out_reg_1_tmp <= to_stdlogicvector(to_bitvector(in_reg_1) sll 1)(92 downto 1) & t3;
    out_reg_2_tmp <= to_stdlogicvector(to_bitvector(in_reg_2) sll 1)(83 downto 1) & t1;
    out_reg_3_tmp <= to_stdlogicvector(to_bitvector(in_reg_3) sll 1)(110 downto 1)& t2;

    out_reg_1 <= out_reg_1_tmp;
    out_reg_2 <= out_reg_2_tmp;
    out_reg_3 <= out_reg_3_tmp;
end architecture ;