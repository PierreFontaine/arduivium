-- Copyright (C) 2018  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 18.1.0 Build 625 09/12/2018 SJ Lite Edition"

-- DATE "08/22/2019 21:13:37"

-- 
-- Device: Altera 10CL016YU256C8G Package UFBGA256
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONE10LP;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONE10LP.CYCLONE10LP_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	MKRVIDOR4000_top IS
    PORT (
	\oHDMI_TX[0](n)\ : OUT std_logic;
	\oHDMI_TX[1](n)\ : OUT std_logic;
	\oHDMI_TX[2](n)\ : OUT std_logic;
	\oHDMI_CLK(n)\ : OUT std_logic;
	\iMIPI_D[0](n)\ : IN std_logic := '0';
	\iMIPI_D[1](n)\ : IN std_logic := '0';
	\iMIPI_CLK(n)\ : IN std_logic := '0';
	iCLK : IN std_logic;
	iRESETn : IN std_logic;
	iSAM_INT : IN std_logic;
	oSAM_INT : BUFFER std_logic;
	oSDRAM_CLK : BUFFER std_logic;
	oSDRAM_ADDR : BUFFER std_logic_vector(11 DOWNTO 0);
	oSDRAM_BA : BUFFER std_logic_vector(1 DOWNTO 0);
	oSDRAM_CASn : BUFFER std_logic;
	oSDRAM_CKE : BUFFER std_logic;
	oSDRAM_CSn : BUFFER std_logic;
	bSDRAM_DQ : BUFFER std_logic_vector(15 DOWNTO 0);
	oSDRAM_DQM : BUFFER std_logic_vector(1 DOWNTO 0);
	oSDRAM_RASn : BUFFER std_logic;
	oSDRAM_WEn : BUFFER std_logic;
	bMKR_AREF : BUFFER std_logic;
	bMKR_A : BUFFER std_logic_vector(6 DOWNTO 0);
	bMKR_D : BUFFER std_logic_vector(14 DOWNTO 0);
	bPEX_RST : BUFFER std_logic;
	bPEX_PIN6 : BUFFER std_logic;
	bPEX_PIN8 : BUFFER std_logic;
	bPEX_PIN10 : BUFFER std_logic;
	iPEX_PIN11 : IN std_logic;
	bPEX_PIN12 : BUFFER std_logic;
	iPEX_PIN13 : IN std_logic;
	bPEX_PIN14 : BUFFER std_logic;
	bPEX_PIN16 : BUFFER std_logic;
	bPEX_PIN20 : BUFFER std_logic;
	iPEX_PIN23 : IN std_logic;
	iPEX_PIN25 : IN std_logic;
	bPEX_PIN28 : BUFFER std_logic;
	bPEX_PIN30 : BUFFER std_logic;
	iPEX_PIN31 : IN std_logic;
	bPEX_PIN32 : BUFFER std_logic;
	iPEX_PIN33 : IN std_logic;
	bPEX_PIN42 : BUFFER std_logic;
	bPEX_PIN44 : BUFFER std_logic;
	bPEX_PIN45 : BUFFER std_logic;
	bPEX_PIN46 : BUFFER std_logic;
	bPEX_PIN47 : BUFFER std_logic;
	bPEX_PIN48 : BUFFER std_logic;
	bPEX_PIN49 : BUFFER std_logic;
	bPEX_PIN51 : BUFFER std_logic;
	bWM_PIO1 : BUFFER std_logic;
	bWM_PIO2 : BUFFER std_logic;
	bWM_PIO3 : BUFFER std_logic;
	bWM_PIO4 : BUFFER std_logic;
	bWM_PIO5 : BUFFER std_logic;
	bWM_PIO7 : BUFFER std_logic;
	bWM_PIO8 : BUFFER std_logic;
	bWM_PIO18 : BUFFER std_logic;
	bWM_PIO20 : BUFFER std_logic;
	bWM_PIO21 : BUFFER std_logic;
	bWM_PIO27 : BUFFER std_logic;
	bWM_PIO28 : BUFFER std_logic;
	bWM_PIO29 : BUFFER std_logic;
	bWM_PIO31 : BUFFER std_logic;
	iWM_PIO32 : IN std_logic;
	bWM_PIO34 : BUFFER std_logic;
	bWM_PIO35 : BUFFER std_logic;
	bWM_PIO36 : BUFFER std_logic;
	iWM_TX : IN std_logic;
	oWM_RX : BUFFER std_logic;
	oWM_RESET : BUFFER std_logic;
	oHDMI_TX : BUFFER std_logic_vector(2 DOWNTO 0);
	oHDMI_CLK : BUFFER std_logic;
	bHDMI_SDA : BUFFER std_logic;
	bHDMI_SCL : BUFFER std_logic;
	iHDMI_HPD : IN std_logic;
	iMIPI_D : IN std_logic_vector(1 DOWNTO 0);
	iMIPI_CLK : IN std_logic;
	bMIPI_SDA : BUFFER std_logic;
	bMIPI_SCL : BUFFER std_logic;
	bMIPI_GP : BUFFER std_logic_vector(1 DOWNTO 0);
	oFLASH_SCK : BUFFER std_logic;
	oFLASH_CS : BUFFER std_logic;
	oFLASH_MOSI : BUFFER std_logic;
	iFLASH_MISO : BUFFER std_logic;
	oFLASH_HOLD : BUFFER std_logic;
	oFLASH_WP : BUFFER std_logic
	);
END MKRVIDOR4000_top;

-- Design Ports Information
-- iRESETn	=>  Location: PIN_E1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- iSAM_INT	=>  Location: PIN_L16,	 I/O Standard: 3.3-V LVCMOS,	 Current Strength: Default
-- oSAM_INT	=>  Location: PIN_N2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- oSDRAM_CLK	=>  Location: PIN_B14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oSDRAM_ADDR[0]	=>  Location: PIN_A12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oSDRAM_ADDR[1]	=>  Location: PIN_B12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oSDRAM_ADDR[2]	=>  Location: PIN_A15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oSDRAM_ADDR[3]	=>  Location: PIN_A14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oSDRAM_ADDR[4]	=>  Location: PIN_D14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oSDRAM_ADDR[5]	=>  Location: PIN_C14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oSDRAM_ADDR[6]	=>  Location: PIN_D11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oSDRAM_ADDR[7]	=>  Location: PIN_D12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oSDRAM_ADDR[8]	=>  Location: PIN_E11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oSDRAM_ADDR[9]	=>  Location: PIN_C9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oSDRAM_ADDR[10]	=>  Location: PIN_B13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oSDRAM_ADDR[11]	=>  Location: PIN_E10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oSDRAM_BA[0]	=>  Location: PIN_A10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oSDRAM_BA[1]	=>  Location: PIN_B10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oSDRAM_CASn	=>  Location: PIN_B7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oSDRAM_CKE	=>  Location: PIN_E9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oSDRAM_CSn	=>  Location: PIN_A11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oSDRAM_DQM[0]	=>  Location: PIN_A7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oSDRAM_DQM[1]	=>  Location: PIN_F9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oSDRAM_RASn	=>  Location: PIN_D9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oSDRAM_WEn	=>  Location: PIN_B11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- iPEX_PIN11	=>  Location: PIN_T8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- iPEX_PIN13	=>  Location: PIN_R8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- iPEX_PIN23	=>  Location: PIN_T9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- iPEX_PIN25	=>  Location: PIN_R9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- iPEX_PIN31	=>  Location: PIN_A9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- iPEX_PIN33	=>  Location: PIN_B9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- iWM_PIO32	=>  Location: PIN_J13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- iWM_TX	=>  Location: PIN_E15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- oHDMI_TX[0]	=>  Location: PIN_R16,	 I/O Standard: LVDS,	 Current Strength: Maximum Current
-- oHDMI_TX[1]	=>  Location: PIN_K15,	 I/O Standard: LVDS,	 Current Strength: Maximum Current
-- oHDMI_TX[2]	=>  Location: PIN_J15,	 I/O Standard: LVDS,	 Current Strength: Maximum Current
-- oHDMI_CLK	=>  Location: PIN_N15,	 I/O Standard: LVDS,	 Current Strength: Maximum Current
-- iHDMI_HPD	=>  Location: PIN_M16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- iMIPI_D[0]	=>  Location: PIN_L2,	 I/O Standard: LVDS,	 Current Strength: Default
-- iMIPI_D[1]	=>  Location: PIN_J2,	 I/O Standard: LVDS,	 Current Strength: Default
-- iMIPI_CLK	=>  Location: PIN_M2,	 I/O Standard: LVDS,	 Current Strength: Default
-- oFLASH_SCK	=>  Location: PIN_H1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- oFLASH_CS	=>  Location: PIN_D2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- bSDRAM_DQ[0]	=>  Location: PIN_A2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bSDRAM_DQ[1]	=>  Location: PIN_B4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bSDRAM_DQ[2]	=>  Location: PIN_B3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bSDRAM_DQ[3]	=>  Location: PIN_A3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bSDRAM_DQ[4]	=>  Location: PIN_A4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bSDRAM_DQ[5]	=>  Location: PIN_A5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bSDRAM_DQ[6]	=>  Location: PIN_B5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bSDRAM_DQ[7]	=>  Location: PIN_A6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bSDRAM_DQ[8]	=>  Location: PIN_F8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bSDRAM_DQ[9]	=>  Location: PIN_C8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bSDRAM_DQ[10]	=>  Location: PIN_E7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bSDRAM_DQ[11]	=>  Location: PIN_E8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bSDRAM_DQ[12]	=>  Location: PIN_E6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bSDRAM_DQ[13]	=>  Location: PIN_D8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bSDRAM_DQ[14]	=>  Location: PIN_D6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bSDRAM_DQ[15]	=>  Location: PIN_B6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bMKR_AREF	=>  Location: PIN_B1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bMKR_A[0]	=>  Location: PIN_C2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bMKR_A[1]	=>  Location: PIN_C3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bMKR_A[2]	=>  Location: PIN_C6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bMKR_A[3]	=>  Location: PIN_D1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bMKR_A[4]	=>  Location: PIN_D3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bMKR_A[5]	=>  Location: PIN_F3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bMKR_A[6]	=>  Location: PIN_G2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bMKR_D[0]	=>  Location: PIN_G1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bMKR_D[2]	=>  Location: PIN_P3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bMKR_D[3]	=>  Location: PIN_R3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bMKR_D[4]	=>  Location: PIN_T3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bMKR_D[5]	=>  Location: PIN_T2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bMKR_D[8]	=>  Location: PIN_F16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bMKR_D[9]	=>  Location: PIN_F15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bMKR_D[10]	=>  Location: PIN_C16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bMKR_D[11]	=>  Location: PIN_C15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bMKR_D[12]	=>  Location: PIN_B16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bMKR_D[13]	=>  Location: PIN_C11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bMKR_D[14]	=>  Location: PIN_A13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bPEX_RST	=>  Location: PIN_T12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bPEX_PIN6	=>  Location: PIN_P8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bPEX_PIN8	=>  Location: PIN_L7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bPEX_PIN10	=>  Location: PIN_N8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bPEX_PIN12	=>  Location: PIN_M8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bPEX_PIN14	=>  Location: PIN_L8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bPEX_PIN16	=>  Location: PIN_M10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bPEX_PIN20	=>  Location: PIN_N12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bPEX_PIN28	=>  Location: PIN_T13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bPEX_PIN30	=>  Location: PIN_R12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bPEX_PIN32	=>  Location: PIN_F13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bPEX_PIN42	=>  Location: PIN_R13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bPEX_PIN44	=>  Location: PIN_P14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bPEX_PIN45	=>  Location: PIN_T15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bPEX_PIN46	=>  Location: PIN_R14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bPEX_PIN47	=>  Location: PIN_T14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bPEX_PIN48	=>  Location: PIN_F14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bPEX_PIN49	=>  Location: PIN_D16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bPEX_PIN51	=>  Location: PIN_D15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bWM_PIO1	=>  Location: PIN_T11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bWM_PIO2	=>  Location: PIN_R10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bWM_PIO3	=>  Location: PIN_P11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bWM_PIO4	=>  Location: PIN_R11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bWM_PIO5	=>  Location: PIN_N6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bWM_PIO7	=>  Location: PIN_P6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bWM_PIO8	=>  Location: PIN_N5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bWM_PIO18	=>  Location: PIN_T5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bWM_PIO20	=>  Location: PIN_R5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bWM_PIO21	=>  Location: PIN_R6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bWM_PIO27	=>  Location: PIN_N9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bWM_PIO28	=>  Location: PIN_N11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bWM_PIO29	=>  Location: PIN_T10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bWM_PIO31	=>  Location: PIN_T4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bWM_PIO34	=>  Location: PIN_M6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bWM_PIO35	=>  Location: PIN_R4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bWM_PIO36	=>  Location: PIN_N1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- oWM_RX	=>  Location: PIN_T6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oWM_RESET	=>  Location: PIN_R1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- bHDMI_SDA	=>  Location: PIN_L4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- bHDMI_SCL	=>  Location: PIN_K5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- bMIPI_SDA	=>  Location: PIN_P2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- bMIPI_SCL	=>  Location: PIN_P1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- bMIPI_GP[0]	=>  Location: PIN_M7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bMIPI_GP[1]	=>  Location: PIN_P9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oFLASH_MOSI	=>  Location: PIN_C1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- iFLASH_MISO	=>  Location: PIN_H2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- oFLASH_HOLD	=>  Location: PIN_R7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- oFLASH_WP	=>  Location: PIN_T7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bMKR_D[1]	=>  Location: PIN_N3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bMKR_D[6]	=>  Location: PIN_G16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- bMKR_D[7]	=>  Location: PIN_G15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 8mA
-- iCLK	=>  Location: PIN_E2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- oHDMI_TX[0](n)	=>  Location: PIN_P16,	 I/O Standard: LVDS,	 Current Strength: Maximum Current
-- oHDMI_TX[1](n)	=>  Location: PIN_K16,	 I/O Standard: LVDS,	 Current Strength: Maximum Current
-- oHDMI_TX[2](n)	=>  Location: PIN_J16,	 I/O Standard: LVDS,	 Current Strength: Maximum Current
-- oHDMI_CLK(n)	=>  Location: PIN_N16,	 I/O Standard: LVDS,	 Current Strength: Maximum Current
-- iMIPI_D[0](n)	=>  Location: PIN_L1,	 I/O Standard: LVDS,	 Current Strength: Default
-- iMIPI_D[1](n)	=>  Location: PIN_J1,	 I/O Standard: LVDS,	 Current Strength: Default
-- iMIPI_CLK(n)	=>  Location: PIN_M1,	 I/O Standard: LVDS,	 Current Strength: Default


ARCHITECTURE structure OF MKRVIDOR4000_top IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \ww_oHDMI_TX[0](n)\ : std_logic;
SIGNAL \ww_oHDMI_TX[1](n)\ : std_logic;
SIGNAL \ww_oHDMI_TX[2](n)\ : std_logic;
SIGNAL \ww_oHDMI_CLK(n)\ : std_logic;
SIGNAL \ww_iMIPI_D[0](n)\ : std_logic;
SIGNAL \ww_iMIPI_D[1](n)\ : std_logic;
SIGNAL \ww_iMIPI_CLK(n)\ : std_logic;
SIGNAL ww_iCLK : std_logic;
SIGNAL ww_iRESETn : std_logic;
SIGNAL ww_iSAM_INT : std_logic;
SIGNAL ww_oSAM_INT : std_logic;
SIGNAL ww_oSDRAM_CLK : std_logic;
SIGNAL ww_oSDRAM_ADDR : std_logic_vector(11 DOWNTO 0);
SIGNAL ww_oSDRAM_BA : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_oSDRAM_CASn : std_logic;
SIGNAL ww_oSDRAM_CKE : std_logic;
SIGNAL ww_oSDRAM_CSn : std_logic;
SIGNAL ww_bSDRAM_DQ : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_oSDRAM_DQM : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_oSDRAM_RASn : std_logic;
SIGNAL ww_oSDRAM_WEn : std_logic;
SIGNAL ww_bMKR_AREF : std_logic;
SIGNAL ww_bMKR_A : std_logic_vector(6 DOWNTO 0);
SIGNAL ww_bMKR_D : std_logic_vector(14 DOWNTO 0);
SIGNAL ww_bPEX_RST : std_logic;
SIGNAL ww_bPEX_PIN6 : std_logic;
SIGNAL ww_bPEX_PIN8 : std_logic;
SIGNAL ww_bPEX_PIN10 : std_logic;
SIGNAL ww_iPEX_PIN11 : std_logic;
SIGNAL ww_bPEX_PIN12 : std_logic;
SIGNAL ww_iPEX_PIN13 : std_logic;
SIGNAL ww_bPEX_PIN14 : std_logic;
SIGNAL ww_bPEX_PIN16 : std_logic;
SIGNAL ww_bPEX_PIN20 : std_logic;
SIGNAL ww_iPEX_PIN23 : std_logic;
SIGNAL ww_iPEX_PIN25 : std_logic;
SIGNAL ww_bPEX_PIN28 : std_logic;
SIGNAL ww_bPEX_PIN30 : std_logic;
SIGNAL ww_iPEX_PIN31 : std_logic;
SIGNAL ww_bPEX_PIN32 : std_logic;
SIGNAL ww_iPEX_PIN33 : std_logic;
SIGNAL ww_bPEX_PIN42 : std_logic;
SIGNAL ww_bPEX_PIN44 : std_logic;
SIGNAL ww_bPEX_PIN45 : std_logic;
SIGNAL ww_bPEX_PIN46 : std_logic;
SIGNAL ww_bPEX_PIN47 : std_logic;
SIGNAL ww_bPEX_PIN48 : std_logic;
SIGNAL ww_bPEX_PIN49 : std_logic;
SIGNAL ww_bPEX_PIN51 : std_logic;
SIGNAL ww_bWM_PIO1 : std_logic;
SIGNAL ww_bWM_PIO2 : std_logic;
SIGNAL ww_bWM_PIO3 : std_logic;
SIGNAL ww_bWM_PIO4 : std_logic;
SIGNAL ww_bWM_PIO5 : std_logic;
SIGNAL ww_bWM_PIO7 : std_logic;
SIGNAL ww_bWM_PIO8 : std_logic;
SIGNAL ww_bWM_PIO18 : std_logic;
SIGNAL ww_bWM_PIO20 : std_logic;
SIGNAL ww_bWM_PIO21 : std_logic;
SIGNAL ww_bWM_PIO27 : std_logic;
SIGNAL ww_bWM_PIO28 : std_logic;
SIGNAL ww_bWM_PIO29 : std_logic;
SIGNAL ww_bWM_PIO31 : std_logic;
SIGNAL ww_iWM_PIO32 : std_logic;
SIGNAL ww_bWM_PIO34 : std_logic;
SIGNAL ww_bWM_PIO35 : std_logic;
SIGNAL ww_bWM_PIO36 : std_logic;
SIGNAL ww_iWM_TX : std_logic;
SIGNAL ww_oWM_RX : std_logic;
SIGNAL ww_oWM_RESET : std_logic;
SIGNAL ww_oHDMI_TX : std_logic_vector(2 DOWNTO 0);
SIGNAL ww_oHDMI_CLK : std_logic;
SIGNAL ww_bHDMI_SDA : std_logic;
SIGNAL ww_bHDMI_SCL : std_logic;
SIGNAL ww_iHDMI_HPD : std_logic;
SIGNAL ww_iMIPI_D : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_iMIPI_CLK : std_logic;
SIGNAL ww_bMIPI_SDA : std_logic;
SIGNAL ww_bMIPI_SCL : std_logic;
SIGNAL ww_bMIPI_GP : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_oFLASH_SCK : std_logic;
SIGNAL ww_oFLASH_CS : std_logic;
SIGNAL ww_oFLASH_MOSI : std_logic;
SIGNAL ww_iFLASH_MISO : std_logic;
SIGNAL ww_oFLASH_HOLD : std_logic;
SIGNAL ww_oFLASH_WP : std_logic;
SIGNAL \PLL_inst|altpll_component|auto_generated|pll1_INCLK_bus\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \PLL_inst|altpll_component|auto_generated|pll1_CLK_bus\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \PLL_inst|altpll_component|auto_generated|wire_pll1_clk[3]~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \wOSC_CLK~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \iRESETn~input_o\ : std_logic;
SIGNAL \iSAM_INT~input_o\ : std_logic;
SIGNAL \iPEX_PIN11~input_o\ : std_logic;
SIGNAL \iPEX_PIN13~input_o\ : std_logic;
SIGNAL \iPEX_PIN23~input_o\ : std_logic;
SIGNAL \iPEX_PIN25~input_o\ : std_logic;
SIGNAL \iPEX_PIN31~input_o\ : std_logic;
SIGNAL \iPEX_PIN33~input_o\ : std_logic;
SIGNAL \iWM_PIO32~input_o\ : std_logic;
SIGNAL \iWM_TX~input_o\ : std_logic;
SIGNAL \iHDMI_HPD~input_o\ : std_logic;
SIGNAL \iMIPI_D[0]~input_o\ : std_logic;
SIGNAL \iMIPI_D[1]~input_o\ : std_logic;
SIGNAL \iMIPI_CLK~input_o\ : std_logic;
SIGNAL \bSDRAM_DQ[0]~input_o\ : std_logic;
SIGNAL \bSDRAM_DQ[1]~input_o\ : std_logic;
SIGNAL \bSDRAM_DQ[2]~input_o\ : std_logic;
SIGNAL \bSDRAM_DQ[3]~input_o\ : std_logic;
SIGNAL \bSDRAM_DQ[4]~input_o\ : std_logic;
SIGNAL \bSDRAM_DQ[5]~input_o\ : std_logic;
SIGNAL \bSDRAM_DQ[6]~input_o\ : std_logic;
SIGNAL \bSDRAM_DQ[7]~input_o\ : std_logic;
SIGNAL \bSDRAM_DQ[8]~input_o\ : std_logic;
SIGNAL \bSDRAM_DQ[9]~input_o\ : std_logic;
SIGNAL \bSDRAM_DQ[10]~input_o\ : std_logic;
SIGNAL \bSDRAM_DQ[11]~input_o\ : std_logic;
SIGNAL \bSDRAM_DQ[12]~input_o\ : std_logic;
SIGNAL \bSDRAM_DQ[13]~input_o\ : std_logic;
SIGNAL \bSDRAM_DQ[14]~input_o\ : std_logic;
SIGNAL \bSDRAM_DQ[15]~input_o\ : std_logic;
SIGNAL \bMKR_AREF~input_o\ : std_logic;
SIGNAL \bMKR_A[0]~input_o\ : std_logic;
SIGNAL \bMKR_A[1]~input_o\ : std_logic;
SIGNAL \bMKR_A[2]~input_o\ : std_logic;
SIGNAL \bMKR_A[3]~input_o\ : std_logic;
SIGNAL \bMKR_A[4]~input_o\ : std_logic;
SIGNAL \bMKR_A[5]~input_o\ : std_logic;
SIGNAL \bMKR_A[6]~input_o\ : std_logic;
SIGNAL \bMKR_D[0]~input_o\ : std_logic;
SIGNAL \bMKR_D[2]~input_o\ : std_logic;
SIGNAL \bMKR_D[3]~input_o\ : std_logic;
SIGNAL \bMKR_D[4]~input_o\ : std_logic;
SIGNAL \bMKR_D[5]~input_o\ : std_logic;
SIGNAL \bMKR_D[8]~input_o\ : std_logic;
SIGNAL \bMKR_D[9]~input_o\ : std_logic;
SIGNAL \bMKR_D[10]~input_o\ : std_logic;
SIGNAL \bMKR_D[11]~input_o\ : std_logic;
SIGNAL \bMKR_D[12]~input_o\ : std_logic;
SIGNAL \bMKR_D[13]~input_o\ : std_logic;
SIGNAL \bMKR_D[14]~input_o\ : std_logic;
SIGNAL \bPEX_RST~input_o\ : std_logic;
SIGNAL \bPEX_PIN6~input_o\ : std_logic;
SIGNAL \bPEX_PIN8~input_o\ : std_logic;
SIGNAL \bPEX_PIN10~input_o\ : std_logic;
SIGNAL \bPEX_PIN12~input_o\ : std_logic;
SIGNAL \bPEX_PIN14~input_o\ : std_logic;
SIGNAL \bPEX_PIN16~input_o\ : std_logic;
SIGNAL \bPEX_PIN20~input_o\ : std_logic;
SIGNAL \bPEX_PIN28~input_o\ : std_logic;
SIGNAL \bPEX_PIN30~input_o\ : std_logic;
SIGNAL \bPEX_PIN32~input_o\ : std_logic;
SIGNAL \bPEX_PIN42~input_o\ : std_logic;
SIGNAL \bPEX_PIN44~input_o\ : std_logic;
SIGNAL \bPEX_PIN45~input_o\ : std_logic;
SIGNAL \bPEX_PIN46~input_o\ : std_logic;
SIGNAL \bPEX_PIN47~input_o\ : std_logic;
SIGNAL \bPEX_PIN48~input_o\ : std_logic;
SIGNAL \bPEX_PIN49~input_o\ : std_logic;
SIGNAL \bPEX_PIN51~input_o\ : std_logic;
SIGNAL \bWM_PIO1~input_o\ : std_logic;
SIGNAL \bWM_PIO2~input_o\ : std_logic;
SIGNAL \bWM_PIO3~input_o\ : std_logic;
SIGNAL \bWM_PIO4~input_o\ : std_logic;
SIGNAL \bWM_PIO5~input_o\ : std_logic;
SIGNAL \bWM_PIO7~input_o\ : std_logic;
SIGNAL \bWM_PIO8~input_o\ : std_logic;
SIGNAL \bWM_PIO18~input_o\ : std_logic;
SIGNAL \bWM_PIO20~input_o\ : std_logic;
SIGNAL \bWM_PIO21~input_o\ : std_logic;
SIGNAL \bWM_PIO27~input_o\ : std_logic;
SIGNAL \bWM_PIO28~input_o\ : std_logic;
SIGNAL \bWM_PIO29~input_o\ : std_logic;
SIGNAL \bWM_PIO31~input_o\ : std_logic;
SIGNAL \bWM_PIO34~input_o\ : std_logic;
SIGNAL \bWM_PIO35~input_o\ : std_logic;
SIGNAL \bWM_PIO36~input_o\ : std_logic;
SIGNAL \oWM_RX~input_o\ : std_logic;
SIGNAL \oWM_RESET~input_o\ : std_logic;
SIGNAL \bHDMI_SDA~input_o\ : std_logic;
SIGNAL \bHDMI_SCL~input_o\ : std_logic;
SIGNAL \bMIPI_SDA~input_o\ : std_logic;
SIGNAL \bMIPI_SCL~input_o\ : std_logic;
SIGNAL \bMIPI_GP[0]~input_o\ : std_logic;
SIGNAL \bMIPI_GP[1]~input_o\ : std_logic;
SIGNAL \oFLASH_MOSI~input_o\ : std_logic;
SIGNAL \iFLASH_MISO~input_o\ : std_logic;
SIGNAL \oFLASH_HOLD~input_o\ : std_logic;
SIGNAL \oFLASH_WP~input_o\ : std_logic;
SIGNAL \bMKR_D[6]~input_o\ : std_logic;
SIGNAL wOSC_CLK : std_logic;
SIGNAL \wOSC_CLK~clkctrl_outclk\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|Add0~5\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|Add0~6_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|prof_divider:count[3]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|Add0~7\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|Add0~8_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|prof_divider:count[4]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|Add0~9\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|Add0~10_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|Equal0~1_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|count~2_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|prof_divider:count[5]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|Add0~11\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|Add0~12_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|count~3_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|prof_divider:count[6]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|Add0~13\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|Add0~14_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|prof_divider:count[7]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|Add0~15\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|Add0~16_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|prof_divider:count[8]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|Add0~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|count~1_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|prof_divider:count[0]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|Add0~1\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|Add0~2_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|prof_divider:count[1]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|Add0~3\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|Add0~4_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|count~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|prof_divider:count[2]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|Equal0~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|s_clk_1hz~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|s_clk_1hz~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\ : std_logic;
SIGNAL \bMKR_D[1]~input_o\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|load~0_combout\ : std_logic;
SIGNAL \bMKR_D[7]~input_o\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.init~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|talu|t3_1~combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][0]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~91_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][1]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~90_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][2]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~89_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][3]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~88_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][4]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~87_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][5]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~86_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][6]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~85_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][7]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~84_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][8]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~83_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][9]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~82_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][10]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~81_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][11]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~80_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][12]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~79_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][13]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~78_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][14]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~77_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][15]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~76_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][16]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~75_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][17]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~74_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][18]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~73_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][19]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~72_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][20]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~71_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][21]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~70_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][22]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~69_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][23]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~68_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][24]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~67_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][25]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~66_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][26]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~65_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][27]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~64_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][28]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~63_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][29]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~62_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][30]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~61_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][31]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~60_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][32]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~59_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][33]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~58_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][34]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~57_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][35]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~56_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][36]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~55_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][37]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~54_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][38]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~53_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][39]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~51_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][40]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~49_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][41]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~47_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][42]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~45_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][43]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~43_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][44]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~41_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][45]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~39_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][46]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~37_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][47]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~35_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][48]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~33_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][49]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~31_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][50]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~29_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][51]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~27_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][52]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~25_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][53]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~23_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][54]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~21_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][55]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~19_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][56]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~17_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][57]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~15_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][58]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~13_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][59]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~11_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][60]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~9_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][61]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~7_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][62]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~5_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][63]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~3_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][64]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~1_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][65]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~52_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][66]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~50_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][67]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~48_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][68]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~46_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][69]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~44_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][70]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~42_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][71]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~40_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][72]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~38_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][73]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~36_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][74]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~34_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][75]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~32_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][76]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~30_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][77]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~28_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][78]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~26_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][79]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~24_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][80]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~22_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][81]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~20_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][82]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~18_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][83]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~16_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][84]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~14_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][85]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~12_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][86]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~10_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][87]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~8_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][88]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~6_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][89]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~4_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][90]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~2_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][91]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][92]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|talu|t1_1~combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][0]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~82_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][1]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~81_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][2]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~80_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][3]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~79_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][4]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~78_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][5]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~77_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][6]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~76_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][7]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~75_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][8]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~74_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][9]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~73_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][10]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~72_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][11]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~71_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][12]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~70_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][13]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~69_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][14]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~68_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][15]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~67_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][16]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~66_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][17]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~65_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][18]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~64_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][19]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~63_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][20]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~62_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][21]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~61_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][22]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~60_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][23]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~59_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][24]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~58_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][25]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~57_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][26]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~56_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][27]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~55_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][28]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~54_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][29]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~53_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][30]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~52_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][31]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~51_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][32]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~50_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][33]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~49_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][34]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~48_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][35]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~47_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][36]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~46_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][37]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~45_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][38]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~44_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][39]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~43_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][40]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~42_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][41]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~41_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][42]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~40_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][43]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~39_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][44]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~38_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][45]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~37_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][46]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~36_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][47]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~35_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][48]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~34_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][49]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~33_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][50]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~32_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][51]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~31_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][52]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~30_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][53]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~29_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][54]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~27_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][55]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~25_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][56]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~23_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][57]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~21_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][58]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~19_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][59]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~17_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][60]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~15_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][61]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~13_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][62]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~11_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][63]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~9_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][64]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~7_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][65]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~5_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][66]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~3_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][67]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~1_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][68]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~28_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][69]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~26_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][70]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~24_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][71]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~22_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][72]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~20_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][73]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~18_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][74]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~16_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][75]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~14_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][76]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~12_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][77]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~10_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][78]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~8_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][79]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~6_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][80]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~4_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][81]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~2_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][82]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][83]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|talu|t2_1~combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][0]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~109_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][1]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~108_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][2]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~107_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][3]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~106_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][4]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~105_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][5]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~104_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][6]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~103_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][7]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~102_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][8]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~101_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][9]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~100_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][10]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~99_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][11]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~98_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][12]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~97_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][13]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~96_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][14]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~95_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][15]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~94_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][16]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~93_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][17]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~92_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][18]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~91_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][19]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~90_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][20]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~89_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][21]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~87_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][22]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~85_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][23]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~83_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][24]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~81_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][25]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~79_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][26]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~77_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][27]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~75_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][28]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~73_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][29]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~71_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][30]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~69_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][31]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~67_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][32]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~65_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][33]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~63_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][34]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~61_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][35]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~59_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][36]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~57_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][37]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~55_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][38]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~53_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][39]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~51_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][40]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~49_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][41]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~47_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][42]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~45_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][43]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~43_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][44]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~41_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][45]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~39_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][46]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~37_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][47]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~35_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][48]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~33_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][49]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~31_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][50]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~29_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][51]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~27_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][52]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~25_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][53]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~23_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][54]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~21_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][55]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~19_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][56]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~17_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][57]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~15_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][58]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~13_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][59]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~11_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][60]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~9_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][61]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~7_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][62]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~5_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][63]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~3_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][64]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~1_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][65]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~88_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][66]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~86_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][67]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~84_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][68]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~82_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][69]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~80_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][70]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~78_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][71]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~76_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][72]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~74_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][73]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~72_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][74]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~70_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][75]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~68_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][76]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~66_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][77]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~64_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][78]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~62_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][79]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~60_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][80]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~58_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][81]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~56_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][82]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~54_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][83]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~52_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][84]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~50_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][85]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~48_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][86]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~46_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][87]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~44_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][88]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~42_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][89]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~40_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][90]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~38_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][91]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~36_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][92]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~34_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][93]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~32_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][94]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~30_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][95]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~28_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][96]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~26_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][97]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~24_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][98]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~22_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][99]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~20_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][100]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~18_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][101]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~16_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][102]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~14_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][103]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~12_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][104]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~10_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][105]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~8_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][106]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~6_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][107]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~4_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][108]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~2_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][109]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|reg|memory[0][110]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|talu|out_z~combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~1_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[31]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector31~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[0]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~1\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~2_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector30~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[1]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~3\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~4_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector29~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[2]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~5\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~6_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector28~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[3]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~7\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~8_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector27~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[4]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~9\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~10_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector26~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[5]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~11\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~12_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector25~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[6]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~13\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~14_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector24~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[7]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~15\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~16_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector23~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[8]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~17\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~18_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector22~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[9]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~19\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~20_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector21~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[10]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~21\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~22_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector20~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[11]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~23\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~24_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector19~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[12]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~25\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~26_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector18~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[13]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~27\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~28_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector17~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[14]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~29\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~30_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector16~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[15]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~31\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~32_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector15~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[16]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~33\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~34_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector14~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[17]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~35\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~36_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector13~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[18]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~37\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~38_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector12~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[19]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~39\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~40_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector11~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[20]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~41\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~42_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector10~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[21]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~43\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~44_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector9~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[22]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~45\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~46_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector8~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[23]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~47\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~48_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector7~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[24]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~49\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~50_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector6~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[25]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~51\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~52_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector5~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[26]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~53\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~55\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~56_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector3~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[28]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~57\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~58_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector2~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[29]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~59\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~60_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector1~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[30]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~61\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~62_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector4~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[27]~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~54_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~6_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~3_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~2_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~1_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~4_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~5_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~7_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~1_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.warmup~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~0_combout\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|bit_ciphered~q\ : std_logic;
SIGNAL \iCLK~input_o\ : std_logic;
SIGNAL \PLL_inst|altpll_component|auto_generated|wire_pll1_fbout\ : std_logic;
SIGNAL \PLL_inst|altpll_component|auto_generated|wire_pll1_clk[3]~clkctrl_outclk\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|talu|out_reg_2\ : std_logic_vector(83 DOWNTO 0);
SIGNAL \TRIV_Schematic_inst|inst|dtm|talu|out_reg_1\ : std_logic_vector(92 DOWNTO 0);
SIGNAL \TRIV_Schematic_inst|inst|dtm|talu|out_reg_3\ : std_logic_vector(110 DOWNTO 0);
SIGNAL \PLL_inst|altpll_component|auto_generated|wire_pll1_clk\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \ALT_INV_bMKR_D[7]~input_o\ : std_logic;
SIGNAL \TRIV_Schematic_inst|inst|dtm|uc_triv|ALT_INV_pr_state.generatekey~q\ : std_logic;

BEGIN

\oHDMI_TX[0](n)\ <= \ww_oHDMI_TX[0](n)\;
\oHDMI_TX[1](n)\ <= \ww_oHDMI_TX[1](n)\;
\oHDMI_TX[2](n)\ <= \ww_oHDMI_TX[2](n)\;
\oHDMI_CLK(n)\ <= \ww_oHDMI_CLK(n)\;
\ww_iMIPI_D[0](n)\ <= \iMIPI_D[0](n)\;
\ww_iMIPI_D[1](n)\ <= \iMIPI_D[1](n)\;
\ww_iMIPI_CLK(n)\ <= \iMIPI_CLK(n)\;
ww_iCLK <= iCLK;
ww_iRESETn <= iRESETn;
ww_iSAM_INT <= iSAM_INT;
oSAM_INT <= ww_oSAM_INT;
oSDRAM_CLK <= ww_oSDRAM_CLK;
oSDRAM_ADDR <= ww_oSDRAM_ADDR;
oSDRAM_BA <= ww_oSDRAM_BA;
oSDRAM_CASn <= ww_oSDRAM_CASn;
oSDRAM_CKE <= ww_oSDRAM_CKE;
oSDRAM_CSn <= ww_oSDRAM_CSn;
bSDRAM_DQ <= ww_bSDRAM_DQ;
oSDRAM_DQM <= ww_oSDRAM_DQM;
oSDRAM_RASn <= ww_oSDRAM_RASn;
oSDRAM_WEn <= ww_oSDRAM_WEn;
bMKR_AREF <= ww_bMKR_AREF;
bMKR_A <= ww_bMKR_A;
bMKR_D <= ww_bMKR_D;
bPEX_RST <= ww_bPEX_RST;
bPEX_PIN6 <= ww_bPEX_PIN6;
bPEX_PIN8 <= ww_bPEX_PIN8;
bPEX_PIN10 <= ww_bPEX_PIN10;
ww_iPEX_PIN11 <= iPEX_PIN11;
bPEX_PIN12 <= ww_bPEX_PIN12;
ww_iPEX_PIN13 <= iPEX_PIN13;
bPEX_PIN14 <= ww_bPEX_PIN14;
bPEX_PIN16 <= ww_bPEX_PIN16;
bPEX_PIN20 <= ww_bPEX_PIN20;
ww_iPEX_PIN23 <= iPEX_PIN23;
ww_iPEX_PIN25 <= iPEX_PIN25;
bPEX_PIN28 <= ww_bPEX_PIN28;
bPEX_PIN30 <= ww_bPEX_PIN30;
ww_iPEX_PIN31 <= iPEX_PIN31;
bPEX_PIN32 <= ww_bPEX_PIN32;
ww_iPEX_PIN33 <= iPEX_PIN33;
bPEX_PIN42 <= ww_bPEX_PIN42;
bPEX_PIN44 <= ww_bPEX_PIN44;
bPEX_PIN45 <= ww_bPEX_PIN45;
bPEX_PIN46 <= ww_bPEX_PIN46;
bPEX_PIN47 <= ww_bPEX_PIN47;
bPEX_PIN48 <= ww_bPEX_PIN48;
bPEX_PIN49 <= ww_bPEX_PIN49;
bPEX_PIN51 <= ww_bPEX_PIN51;
bWM_PIO1 <= ww_bWM_PIO1;
bWM_PIO2 <= ww_bWM_PIO2;
bWM_PIO3 <= ww_bWM_PIO3;
bWM_PIO4 <= ww_bWM_PIO4;
bWM_PIO5 <= ww_bWM_PIO5;
bWM_PIO7 <= ww_bWM_PIO7;
bWM_PIO8 <= ww_bWM_PIO8;
bWM_PIO18 <= ww_bWM_PIO18;
bWM_PIO20 <= ww_bWM_PIO20;
bWM_PIO21 <= ww_bWM_PIO21;
bWM_PIO27 <= ww_bWM_PIO27;
bWM_PIO28 <= ww_bWM_PIO28;
bWM_PIO29 <= ww_bWM_PIO29;
bWM_PIO31 <= ww_bWM_PIO31;
ww_iWM_PIO32 <= iWM_PIO32;
bWM_PIO34 <= ww_bWM_PIO34;
bWM_PIO35 <= ww_bWM_PIO35;
bWM_PIO36 <= ww_bWM_PIO36;
ww_iWM_TX <= iWM_TX;
oWM_RX <= ww_oWM_RX;
oWM_RESET <= ww_oWM_RESET;
oHDMI_TX <= ww_oHDMI_TX;
oHDMI_CLK <= ww_oHDMI_CLK;
bHDMI_SDA <= ww_bHDMI_SDA;
bHDMI_SCL <= ww_bHDMI_SCL;
ww_iHDMI_HPD <= iHDMI_HPD;
ww_iMIPI_D <= iMIPI_D;
ww_iMIPI_CLK <= iMIPI_CLK;
bMIPI_SDA <= ww_bMIPI_SDA;
bMIPI_SCL <= ww_bMIPI_SCL;
bMIPI_GP <= ww_bMIPI_GP;
oFLASH_SCK <= ww_oFLASH_SCK;
oFLASH_CS <= ww_oFLASH_CS;
oFLASH_MOSI <= ww_oFLASH_MOSI;
iFLASH_MISO <= ww_iFLASH_MISO;
oFLASH_HOLD <= ww_oFLASH_HOLD;
oFLASH_WP <= ww_oFLASH_WP;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\PLL_inst|altpll_component|auto_generated|pll1_INCLK_bus\ <= (gnd & \iCLK~input_o\);

\PLL_inst|altpll_component|auto_generated|wire_pll1_clk\(0) <= \PLL_inst|altpll_component|auto_generated|pll1_CLK_bus\(0);
\PLL_inst|altpll_component|auto_generated|wire_pll1_clk\(1) <= \PLL_inst|altpll_component|auto_generated|pll1_CLK_bus\(1);
\PLL_inst|altpll_component|auto_generated|wire_pll1_clk\(2) <= \PLL_inst|altpll_component|auto_generated|pll1_CLK_bus\(2);
\PLL_inst|altpll_component|auto_generated|wire_pll1_clk\(3) <= \PLL_inst|altpll_component|auto_generated|pll1_CLK_bus\(3);
\PLL_inst|altpll_component|auto_generated|wire_pll1_clk\(4) <= \PLL_inst|altpll_component|auto_generated|pll1_CLK_bus\(4);

\TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \TRIV_Schematic_inst|inst|s_clk_1hz~q\);

\PLL_inst|altpll_component|auto_generated|wire_pll1_clk[3]~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \PLL_inst|altpll_component|auto_generated|wire_pll1_clk\(3));

\wOSC_CLK~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & wOSC_CLK);
\ALT_INV_bMKR_D[7]~input_o\ <= NOT \bMKR_D[7]~input_o\;
\TRIV_Schematic_inst|inst|dtm|uc_triv|ALT_INV_pr_state.generatekey~q\ <= NOT \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\;

-- Location: IOOBUF_X0_Y5_N9
\oSAM_INT~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oSAM_INT);

-- Location: IOOBUF_X35_Y29_N9
\oSDRAM_CLK~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \PLL_inst|altpll_component|auto_generated|wire_pll1_clk[3]~clkctrl_outclk\,
	devoe => ww_devoe,
	o => ww_oSDRAM_CLK);

-- Location: IOOBUF_X32_Y29_N23
\oSDRAM_ADDR[0]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oSDRAM_ADDR(0));

-- Location: IOOBUF_X32_Y29_N30
\oSDRAM_ADDR[1]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oSDRAM_ADDR(1));

-- Location: IOOBUF_X28_Y29_N16
\oSDRAM_ADDR[2]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oSDRAM_ADDR(2));

-- Location: IOOBUF_X35_Y29_N2
\oSDRAM_ADDR[3]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oSDRAM_ADDR(3));

-- Location: IOOBUF_X39_Y29_N9
\oSDRAM_ADDR[4]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oSDRAM_ADDR(4));

-- Location: IOOBUF_X39_Y29_N2
\oSDRAM_ADDR[5]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oSDRAM_ADDR(5));

-- Location: IOOBUF_X39_Y29_N30
\oSDRAM_ADDR[6]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oSDRAM_ADDR(6));

-- Location: IOOBUF_X37_Y29_N2
\oSDRAM_ADDR[7]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oSDRAM_ADDR(7));

-- Location: IOOBUF_X32_Y29_N2
\oSDRAM_ADDR[8]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oSDRAM_ADDR(8));

-- Location: IOOBUF_X23_Y29_N2
\oSDRAM_ADDR[9]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oSDRAM_ADDR(9));

-- Location: IOOBUF_X37_Y29_N23
\oSDRAM_ADDR[10]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oSDRAM_ADDR(10));

-- Location: IOOBUF_X32_Y29_N9
\oSDRAM_ADDR[11]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oSDRAM_ADDR(11));

-- Location: IOOBUF_X26_Y29_N23
\oSDRAM_BA[0]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oSDRAM_BA(0));

-- Location: IOOBUF_X26_Y29_N30
\oSDRAM_BA[1]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oSDRAM_BA(1));

-- Location: IOOBUF_X11_Y29_N9
\oSDRAM_CASn~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oSDRAM_CASn);

-- Location: IOOBUF_X21_Y29_N9
\oSDRAM_CKE~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oSDRAM_CKE);

-- Location: IOOBUF_X30_Y29_N16
\oSDRAM_CSn~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oSDRAM_CSn);

-- Location: IOOBUF_X11_Y29_N2
\oSDRAM_DQM[0]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oSDRAM_DQM(0));

-- Location: IOOBUF_X26_Y29_N16
\oSDRAM_DQM[1]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oSDRAM_DQM(1));

-- Location: IOOBUF_X23_Y29_N9
\oSDRAM_RASn~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oSDRAM_RASn);

-- Location: IOOBUF_X30_Y29_N23
\oSDRAM_WEn~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oSDRAM_WEn);

-- Location: IOOBUF_X41_Y3_N9
\oHDMI_TX[0]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oHDMI_TX(0),
	obar => \ww_oHDMI_TX[0](n)\);

-- Location: IOOBUF_X41_Y13_N23
\oHDMI_TX[1]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oHDMI_TX(1),
	obar => \ww_oHDMI_TX[1](n)\);

-- Location: IOOBUF_X41_Y13_N9
\oHDMI_TX[2]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oHDMI_TX(2),
	obar => \ww_oHDMI_TX[2](n)\);

-- Location: IOOBUF_X41_Y5_N2
\oHDMI_CLK~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oHDMI_CLK,
	obar => \ww_oHDMI_CLK(n)\);

-- Location: IOOBUF_X0_Y20_N16
\oFLASH_SCK~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oFLASH_SCK);

-- Location: IOOBUF_X0_Y24_N9
\oFLASH_CS~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_oFLASH_CS);

-- Location: IOOBUF_X3_Y29_N16
\bSDRAM_DQ[0]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bSDRAM_DQ(0));

-- Location: IOOBUF_X3_Y29_N9
\bSDRAM_DQ[1]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bSDRAM_DQ(1));

-- Location: IOOBUF_X1_Y29_N2
\bSDRAM_DQ[2]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bSDRAM_DQ(2));

-- Location: IOOBUF_X3_Y29_N30
\bSDRAM_DQ[3]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bSDRAM_DQ(3));

-- Location: IOOBUF_X3_Y29_N2
\bSDRAM_DQ[4]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bSDRAM_DQ(4));

-- Location: IOOBUF_X5_Y29_N2
\bSDRAM_DQ[5]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bSDRAM_DQ(5));

-- Location: IOOBUF_X5_Y29_N16
\bSDRAM_DQ[6]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bSDRAM_DQ(6));

-- Location: IOOBUF_X9_Y29_N2
\bSDRAM_DQ[7]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bSDRAM_DQ(7));

-- Location: IOOBUF_X14_Y29_N30
\bSDRAM_DQ[8]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bSDRAM_DQ(8));

-- Location: IOOBUF_X14_Y29_N2
\bSDRAM_DQ[9]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bSDRAM_DQ(9));

-- Location: IOOBUF_X7_Y29_N9
\bSDRAM_DQ[10]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bSDRAM_DQ(10));

-- Location: IOOBUF_X14_Y29_N23
\bSDRAM_DQ[11]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bSDRAM_DQ(11));

-- Location: IOOBUF_X7_Y29_N30
\bSDRAM_DQ[12]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bSDRAM_DQ(12));

-- Location: IOOBUF_X14_Y29_N9
\bSDRAM_DQ[13]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bSDRAM_DQ(13));

-- Location: IOOBUF_X5_Y29_N23
\bSDRAM_DQ[14]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bSDRAM_DQ(14));

-- Location: IOOBUF_X9_Y29_N9
\bSDRAM_DQ[15]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bSDRAM_DQ(15));

-- Location: IOOBUF_X0_Y26_N16
\bMKR_AREF~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMKR_AREF);

-- Location: IOOBUF_X0_Y25_N2
\bMKR_A[0]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMKR_A(0));

-- Location: IOOBUF_X1_Y29_N23
\bMKR_A[1]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMKR_A(1));

-- Location: IOOBUF_X11_Y29_N30
\bMKR_A[2]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMKR_A(2));

-- Location: IOOBUF_X0_Y24_N16
\bMKR_A[3]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMKR_A(3));

-- Location: IOOBUF_X1_Y29_N30
\bMKR_A[4]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMKR_A(4));

-- Location: IOOBUF_X0_Y25_N16
\bMKR_A[5]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMKR_A(5));

-- Location: IOOBUF_X0_Y21_N2
\bMKR_A[6]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMKR_A(6));

-- Location: IOOBUF_X0_Y21_N23
\bMKR_D[0]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMKR_D(0));

-- Location: IOOBUF_X3_Y0_N30
\bMKR_D[2]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMKR_D(2));

-- Location: IOOBUF_X3_Y0_N16
\bMKR_D[3]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMKR_D(3));

-- Location: IOOBUF_X3_Y0_N9
\bMKR_D[4]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMKR_D(4));

-- Location: IOOBUF_X5_Y0_N9
\bMKR_D[5]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMKR_D(5));

-- Location: IOOBUF_X41_Y19_N16
\bMKR_D[8]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMKR_D(8));

-- Location: IOOBUF_X41_Y19_N9
\bMKR_D[9]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMKR_D(9));

-- Location: IOOBUF_X41_Y27_N23
\bMKR_D[10]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMKR_D(10));

-- Location: IOOBUF_X41_Y27_N16
\bMKR_D[11]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMKR_D(11));

-- Location: IOOBUF_X41_Y19_N2
\bMKR_D[12]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMKR_D(12));

-- Location: IOOBUF_X37_Y29_N16
\bMKR_D[13]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMKR_D(13));

-- Location: IOOBUF_X28_Y29_N9
\bMKR_D[14]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMKR_D(14));

-- Location: IOOBUF_X28_Y0_N23
\bPEX_RST~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bPEX_RST);

-- Location: IOOBUF_X21_Y0_N30
\bPEX_PIN6~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bPEX_PIN6);

-- Location: IOOBUF_X16_Y0_N16
\bPEX_PIN8~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bPEX_PIN8);

-- Location: IOOBUF_X19_Y0_N2
\bPEX_PIN10~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bPEX_PIN10);

-- Location: IOOBUF_X19_Y0_N9
\bPEX_PIN12~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bPEX_PIN12);

-- Location: IOOBUF_X19_Y0_N30
\bPEX_PIN14~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bPEX_PIN14);

-- Location: IOOBUF_X35_Y0_N23
\bPEX_PIN16~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bPEX_PIN16);

-- Location: IOOBUF_X30_Y0_N16
\bPEX_PIN20~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bPEX_PIN20);

-- Location: IOOBUF_X30_Y0_N2
\bPEX_PIN28~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bPEX_PIN28);

-- Location: IOOBUF_X26_Y0_N2
\bPEX_PIN30~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bPEX_PIN30);

-- Location: IOOBUF_X41_Y18_N2
\bPEX_PIN32~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bPEX_PIN32);

-- Location: IOOBUF_X30_Y0_N9
\bPEX_PIN42~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bPEX_PIN42);

-- Location: IOOBUF_X37_Y0_N9
\bPEX_PIN44~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bPEX_PIN44);

-- Location: IOOBUF_X35_Y0_N2
\bPEX_PIN45~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bPEX_PIN45);

-- Location: IOOBUF_X37_Y0_N2
\bPEX_PIN46~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bPEX_PIN46);

-- Location: IOOBUF_X35_Y0_N9
\bPEX_PIN47~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bPEX_PIN47);

-- Location: IOOBUF_X41_Y23_N2
\bPEX_PIN48~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bPEX_PIN48);

-- Location: IOOBUF_X41_Y24_N9
\bPEX_PIN49~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bPEX_PIN49);

-- Location: IOOBUF_X41_Y24_N2
\bPEX_PIN51~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bPEX_PIN51);

-- Location: IOOBUF_X26_Y0_N9
\bWM_PIO1~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bWM_PIO1);

-- Location: IOOBUF_X26_Y0_N30
\bWM_PIO2~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bWM_PIO2);

-- Location: IOOBUF_X37_Y0_N30
\bWM_PIO3~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bWM_PIO3);

-- Location: IOOBUF_X26_Y0_N16
\bWM_PIO4~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bWM_PIO4);

-- Location: IOOBUF_X7_Y0_N16
\bWM_PIO5~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bWM_PIO5);

-- Location: IOOBUF_X14_Y0_N23
\bWM_PIO7~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bWM_PIO7);

-- Location: IOOBUF_X7_Y0_N23
\bWM_PIO8~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bWM_PIO8);

-- Location: IOOBUF_X14_Y0_N2
\bWM_PIO18~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bWM_PIO18);

-- Location: IOOBUF_X14_Y0_N9
\bWM_PIO20~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bWM_PIO20);

-- Location: IOOBUF_X16_Y0_N30
\bWM_PIO21~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bWM_PIO21);

-- Location: IOOBUF_X23_Y0_N9
\bWM_PIO27~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bWM_PIO27);

-- Location: IOOBUF_X35_Y0_N16
\bWM_PIO28~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bWM_PIO28);

-- Location: IOOBUF_X26_Y0_N23
\bWM_PIO29~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bWM_PIO29);

-- Location: IOOBUF_X7_Y0_N30
\bWM_PIO31~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bWM_PIO31);

-- Location: IOOBUF_X7_Y0_N9
\bWM_PIO34~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bWM_PIO34);

-- Location: IOOBUF_X5_Y0_N2
\bWM_PIO35~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bWM_PIO35);

-- Location: IOOBUF_X0_Y5_N16
\bWM_PIO36~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bWM_PIO36);

-- Location: IOOBUF_X16_Y0_N23
\oWM_RX~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_oWM_RX);

-- Location: IOOBUF_X0_Y4_N23
\oWM_RESET~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_oWM_RESET);

-- Location: IOOBUF_X0_Y4_N2
\bHDMI_SDA~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bHDMI_SDA);

-- Location: IOOBUF_X0_Y5_N23
\bHDMI_SCL~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bHDMI_SCL);

-- Location: IOOBUF_X0_Y3_N2
\bMIPI_SDA~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMIPI_SDA);

-- Location: IOOBUF_X0_Y3_N9
\bMIPI_SCL~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMIPI_SCL);

-- Location: IOOBUF_X14_Y0_N16
\bMIPI_GP[0]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMIPI_GP(0));

-- Location: IOOBUF_X30_Y0_N23
\bMIPI_GP[1]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMIPI_GP(1));

-- Location: IOOBUF_X0_Y25_N9
\oFLASH_MOSI~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_oFLASH_MOSI);

-- Location: IOOBUF_X0_Y20_N23
\iFLASH_MISO~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_iFLASH_MISO);

-- Location: IOOBUF_X16_Y0_N9
\oFLASH_HOLD~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_oFLASH_HOLD);

-- Location: IOOBUF_X16_Y0_N2
\oFLASH_WP~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_oFLASH_WP);

-- Location: IOOBUF_X1_Y0_N2
\bMKR_D[1]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMKR_D(1));

-- Location: IOOBUF_X41_Y18_N23
\bMKR_D[6]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \TRIV_Schematic_inst|inst|dtm|uc_triv|bit_ciphered~q\,
	oe => VCC,
	devoe => ww_devoe,
	o => ww_bMKR_D(6));

-- Location: IOOBUF_X41_Y18_N16
\bMKR_D[7]~output\ : cyclone10lp_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "true")
-- pragma translate_on
PORT MAP (
	i => VCC,
	devoe => ww_devoe,
	o => ww_bMKR_D(7));

-- Location: OSCILLATOR_X1_Y15_N3
osc : cyclone10lp_oscillator
PORT MAP (
	oscena => VCC,
	clkout => wOSC_CLK);

-- Location: CLKCTRL_G0
\wOSC_CLK~clkctrl\ : cyclone10lp_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \wOSC_CLK~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \wOSC_CLK~clkctrl_outclk\);

-- Location: LCCOMB_X19_Y28_N12
\TRIV_Schematic_inst|inst|Add0~4\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|Add0~4_combout\ = (\TRIV_Schematic_inst|inst|prof_divider:count[2]~q\ & (\TRIV_Schematic_inst|inst|Add0~3\ $ (GND))) # (!\TRIV_Schematic_inst|inst|prof_divider:count[2]~q\ & (!\TRIV_Schematic_inst|inst|Add0~3\ & VCC))
-- \TRIV_Schematic_inst|inst|Add0~5\ = CARRY((\TRIV_Schematic_inst|inst|prof_divider:count[2]~q\ & !\TRIV_Schematic_inst|inst|Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|prof_divider:count[2]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|Add0~3\,
	combout => \TRIV_Schematic_inst|inst|Add0~4_combout\,
	cout => \TRIV_Schematic_inst|inst|Add0~5\);

-- Location: LCCOMB_X19_Y28_N14
\TRIV_Schematic_inst|inst|Add0~6\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|Add0~6_combout\ = (\TRIV_Schematic_inst|inst|prof_divider:count[3]~q\ & (!\TRIV_Schematic_inst|inst|Add0~5\)) # (!\TRIV_Schematic_inst|inst|prof_divider:count[3]~q\ & ((\TRIV_Schematic_inst|inst|Add0~5\) # (GND)))
-- \TRIV_Schematic_inst|inst|Add0~7\ = CARRY((!\TRIV_Schematic_inst|inst|Add0~5\) # (!\TRIV_Schematic_inst|inst|prof_divider:count[3]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|prof_divider:count[3]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|Add0~5\,
	combout => \TRIV_Schematic_inst|inst|Add0~6_combout\,
	cout => \TRIV_Schematic_inst|inst|Add0~7\);

-- Location: FF_X19_Y28_N15
\TRIV_Schematic_inst|inst|prof_divider:count[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wOSC_CLK~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|Add0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|prof_divider:count[3]~q\);

-- Location: LCCOMB_X19_Y28_N16
\TRIV_Schematic_inst|inst|Add0~8\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|Add0~8_combout\ = (\TRIV_Schematic_inst|inst|prof_divider:count[4]~q\ & (\TRIV_Schematic_inst|inst|Add0~7\ $ (GND))) # (!\TRIV_Schematic_inst|inst|prof_divider:count[4]~q\ & (!\TRIV_Schematic_inst|inst|Add0~7\ & VCC))
-- \TRIV_Schematic_inst|inst|Add0~9\ = CARRY((\TRIV_Schematic_inst|inst|prof_divider:count[4]~q\ & !\TRIV_Schematic_inst|inst|Add0~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|prof_divider:count[4]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|Add0~7\,
	combout => \TRIV_Schematic_inst|inst|Add0~8_combout\,
	cout => \TRIV_Schematic_inst|inst|Add0~9\);

-- Location: FF_X19_Y28_N17
\TRIV_Schematic_inst|inst|prof_divider:count[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wOSC_CLK~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|Add0~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|prof_divider:count[4]~q\);

-- Location: LCCOMB_X19_Y28_N18
\TRIV_Schematic_inst|inst|Add0~10\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|Add0~10_combout\ = (\TRIV_Schematic_inst|inst|prof_divider:count[5]~q\ & (!\TRIV_Schematic_inst|inst|Add0~9\)) # (!\TRIV_Schematic_inst|inst|prof_divider:count[5]~q\ & ((\TRIV_Schematic_inst|inst|Add0~9\) # (GND)))
-- \TRIV_Schematic_inst|inst|Add0~11\ = CARRY((!\TRIV_Schematic_inst|inst|Add0~9\) # (!\TRIV_Schematic_inst|inst|prof_divider:count[5]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|prof_divider:count[5]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|Add0~9\,
	combout => \TRIV_Schematic_inst|inst|Add0~10_combout\,
	cout => \TRIV_Schematic_inst|inst|Add0~11\);

-- Location: LCCOMB_X19_Y28_N2
\TRIV_Schematic_inst|inst|Equal0~1\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|Equal0~1_combout\ = (\TRIV_Schematic_inst|inst|prof_divider:count[5]~q\ & (\TRIV_Schematic_inst|inst|prof_divider:count[6]~q\ & (!\TRIV_Schematic_inst|inst|prof_divider:count[7]~q\ & 
-- !\TRIV_Schematic_inst|inst|prof_divider:count[4]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|prof_divider:count[5]~q\,
	datab => \TRIV_Schematic_inst|inst|prof_divider:count[6]~q\,
	datac => \TRIV_Schematic_inst|inst|prof_divider:count[7]~q\,
	datad => \TRIV_Schematic_inst|inst|prof_divider:count[4]~q\,
	combout => \TRIV_Schematic_inst|inst|Equal0~1_combout\);

-- Location: LCCOMB_X19_Y28_N6
\TRIV_Schematic_inst|inst|count~2\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|count~2_combout\ = (\TRIV_Schematic_inst|inst|Add0~10_combout\ & ((\TRIV_Schematic_inst|inst|prof_divider:count[8]~q\) # ((!\TRIV_Schematic_inst|inst|Equal0~1_combout\) # (!\TRIV_Schematic_inst|inst|Equal0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|Add0~10_combout\,
	datab => \TRIV_Schematic_inst|inst|prof_divider:count[8]~q\,
	datac => \TRIV_Schematic_inst|inst|Equal0~0_combout\,
	datad => \TRIV_Schematic_inst|inst|Equal0~1_combout\,
	combout => \TRIV_Schematic_inst|inst|count~2_combout\);

-- Location: FF_X19_Y28_N7
\TRIV_Schematic_inst|inst|prof_divider:count[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wOSC_CLK~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|count~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|prof_divider:count[5]~q\);

-- Location: LCCOMB_X19_Y28_N20
\TRIV_Schematic_inst|inst|Add0~12\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|Add0~12_combout\ = (\TRIV_Schematic_inst|inst|prof_divider:count[6]~q\ & (\TRIV_Schematic_inst|inst|Add0~11\ $ (GND))) # (!\TRIV_Schematic_inst|inst|prof_divider:count[6]~q\ & (!\TRIV_Schematic_inst|inst|Add0~11\ & VCC))
-- \TRIV_Schematic_inst|inst|Add0~13\ = CARRY((\TRIV_Schematic_inst|inst|prof_divider:count[6]~q\ & !\TRIV_Schematic_inst|inst|Add0~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|prof_divider:count[6]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|Add0~11\,
	combout => \TRIV_Schematic_inst|inst|Add0~12_combout\,
	cout => \TRIV_Schematic_inst|inst|Add0~13\);

-- Location: LCCOMB_X19_Y28_N28
\TRIV_Schematic_inst|inst|count~3\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|count~3_combout\ = (\TRIV_Schematic_inst|inst|Add0~12_combout\ & ((\TRIV_Schematic_inst|inst|prof_divider:count[8]~q\) # ((!\TRIV_Schematic_inst|inst|Equal0~1_combout\) # (!\TRIV_Schematic_inst|inst|Equal0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|Add0~12_combout\,
	datab => \TRIV_Schematic_inst|inst|prof_divider:count[8]~q\,
	datac => \TRIV_Schematic_inst|inst|Equal0~0_combout\,
	datad => \TRIV_Schematic_inst|inst|Equal0~1_combout\,
	combout => \TRIV_Schematic_inst|inst|count~3_combout\);

-- Location: FF_X19_Y28_N29
\TRIV_Schematic_inst|inst|prof_divider:count[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wOSC_CLK~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|count~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|prof_divider:count[6]~q\);

-- Location: LCCOMB_X19_Y28_N22
\TRIV_Schematic_inst|inst|Add0~14\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|Add0~14_combout\ = (\TRIV_Schematic_inst|inst|prof_divider:count[7]~q\ & (!\TRIV_Schematic_inst|inst|Add0~13\)) # (!\TRIV_Schematic_inst|inst|prof_divider:count[7]~q\ & ((\TRIV_Schematic_inst|inst|Add0~13\) # (GND)))
-- \TRIV_Schematic_inst|inst|Add0~15\ = CARRY((!\TRIV_Schematic_inst|inst|Add0~13\) # (!\TRIV_Schematic_inst|inst|prof_divider:count[7]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|prof_divider:count[7]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|Add0~13\,
	combout => \TRIV_Schematic_inst|inst|Add0~14_combout\,
	cout => \TRIV_Schematic_inst|inst|Add0~15\);

-- Location: FF_X19_Y28_N23
\TRIV_Schematic_inst|inst|prof_divider:count[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wOSC_CLK~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|Add0~14_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|prof_divider:count[7]~q\);

-- Location: LCCOMB_X19_Y28_N24
\TRIV_Schematic_inst|inst|Add0~16\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|Add0~16_combout\ = \TRIV_Schematic_inst|inst|Add0~15\ $ (!\TRIV_Schematic_inst|inst|prof_divider:count[8]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \TRIV_Schematic_inst|inst|prof_divider:count[8]~q\,
	cin => \TRIV_Schematic_inst|inst|Add0~15\,
	combout => \TRIV_Schematic_inst|inst|Add0~16_combout\);

-- Location: FF_X19_Y28_N25
\TRIV_Schematic_inst|inst|prof_divider:count[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wOSC_CLK~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|Add0~16_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|prof_divider:count[8]~q\);

-- Location: LCCOMB_X19_Y28_N8
\TRIV_Schematic_inst|inst|Add0~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|Add0~0_combout\ = \TRIV_Schematic_inst|inst|prof_divider:count[0]~q\ $ (VCC)
-- \TRIV_Schematic_inst|inst|Add0~1\ = CARRY(\TRIV_Schematic_inst|inst|prof_divider:count[0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|prof_divider:count[0]~q\,
	datad => VCC,
	combout => \TRIV_Schematic_inst|inst|Add0~0_combout\,
	cout => \TRIV_Schematic_inst|inst|Add0~1\);

-- Location: LCCOMB_X19_Y28_N4
\TRIV_Schematic_inst|inst|count~1\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|count~1_combout\ = (\TRIV_Schematic_inst|inst|Add0~0_combout\ & (((\TRIV_Schematic_inst|inst|prof_divider:count[8]~q\) # (!\TRIV_Schematic_inst|inst|Equal0~1_combout\)) # (!\TRIV_Schematic_inst|inst|Equal0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|Equal0~0_combout\,
	datab => \TRIV_Schematic_inst|inst|prof_divider:count[8]~q\,
	datac => \TRIV_Schematic_inst|inst|Add0~0_combout\,
	datad => \TRIV_Schematic_inst|inst|Equal0~1_combout\,
	combout => \TRIV_Schematic_inst|inst|count~1_combout\);

-- Location: FF_X19_Y28_N5
\TRIV_Schematic_inst|inst|prof_divider:count[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wOSC_CLK~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|count~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|prof_divider:count[0]~q\);

-- Location: LCCOMB_X19_Y28_N10
\TRIV_Schematic_inst|inst|Add0~2\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|Add0~2_combout\ = (\TRIV_Schematic_inst|inst|prof_divider:count[1]~q\ & (!\TRIV_Schematic_inst|inst|Add0~1\)) # (!\TRIV_Schematic_inst|inst|prof_divider:count[1]~q\ & ((\TRIV_Schematic_inst|inst|Add0~1\) # (GND)))
-- \TRIV_Schematic_inst|inst|Add0~3\ = CARRY((!\TRIV_Schematic_inst|inst|Add0~1\) # (!\TRIV_Schematic_inst|inst|prof_divider:count[1]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|prof_divider:count[1]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|Add0~1\,
	combout => \TRIV_Schematic_inst|inst|Add0~2_combout\,
	cout => \TRIV_Schematic_inst|inst|Add0~3\);

-- Location: FF_X19_Y28_N11
\TRIV_Schematic_inst|inst|prof_divider:count[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wOSC_CLK~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|Add0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|prof_divider:count[1]~q\);

-- Location: LCCOMB_X19_Y28_N26
\TRIV_Schematic_inst|inst|count~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|count~0_combout\ = (\TRIV_Schematic_inst|inst|Add0~4_combout\ & ((\TRIV_Schematic_inst|inst|prof_divider:count[8]~q\) # ((!\TRIV_Schematic_inst|inst|Equal0~1_combout\) # (!\TRIV_Schematic_inst|inst|Equal0~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|Add0~4_combout\,
	datab => \TRIV_Schematic_inst|inst|prof_divider:count[8]~q\,
	datac => \TRIV_Schematic_inst|inst|Equal0~0_combout\,
	datad => \TRIV_Schematic_inst|inst|Equal0~1_combout\,
	combout => \TRIV_Schematic_inst|inst|count~0_combout\);

-- Location: FF_X19_Y28_N27
\TRIV_Schematic_inst|inst|prof_divider:count[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wOSC_CLK~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|count~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|prof_divider:count[2]~q\);

-- Location: LCCOMB_X19_Y28_N30
\TRIV_Schematic_inst|inst|Equal0~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|Equal0~0_combout\ = (\TRIV_Schematic_inst|inst|prof_divider:count[2]~q\ & (!\TRIV_Schematic_inst|inst|prof_divider:count[3]~q\ & (!\TRIV_Schematic_inst|inst|prof_divider:count[0]~q\ & 
-- !\TRIV_Schematic_inst|inst|prof_divider:count[1]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|prof_divider:count[2]~q\,
	datab => \TRIV_Schematic_inst|inst|prof_divider:count[3]~q\,
	datac => \TRIV_Schematic_inst|inst|prof_divider:count[0]~q\,
	datad => \TRIV_Schematic_inst|inst|prof_divider:count[1]~q\,
	combout => \TRIV_Schematic_inst|inst|Equal0~0_combout\);

-- Location: LCCOMB_X19_Y28_N0
\TRIV_Schematic_inst|inst|s_clk_1hz~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|s_clk_1hz~0_combout\ = \TRIV_Schematic_inst|inst|s_clk_1hz~q\ $ (((\TRIV_Schematic_inst|inst|Equal0~0_combout\ & (!\TRIV_Schematic_inst|inst|prof_divider:count[8]~q\ & \TRIV_Schematic_inst|inst|Equal0~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101001011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|Equal0~0_combout\,
	datab => \TRIV_Schematic_inst|inst|prof_divider:count[8]~q\,
	datac => \TRIV_Schematic_inst|inst|s_clk_1hz~q\,
	datad => \TRIV_Schematic_inst|inst|Equal0~1_combout\,
	combout => \TRIV_Schematic_inst|inst|s_clk_1hz~0_combout\);

-- Location: FF_X19_Y28_N1
\TRIV_Schematic_inst|inst|s_clk_1hz\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \wOSC_CLK~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|s_clk_1hz~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|s_clk_1hz~q\);

-- Location: CLKCTRL_G10
\TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl\ : cyclone10lp_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\);

-- Location: IOIBUF_X1_Y0_N1
\bMKR_D[1]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMKR_D(1),
	o => \bMKR_D[1]~input_o\);

-- Location: LCCOMB_X39_Y17_N14
\TRIV_Schematic_inst|inst|dtm|uc_triv|load~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|load~0_combout\ = (\bMKR_D[1]~input_o\) # (\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110011111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \bMKR_D[1]~input_o\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~0_combout\);

-- Location: IOIBUF_X41_Y18_N15
\bMKR_D[7]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMKR_D(7),
	o => \bMKR_D[7]~input_o\);

-- Location: FF_X39_Y17_N15
\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.init\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.init~q\);

-- Location: LCCOMB_X39_Y17_N26
\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~0_combout\ = (\bMKR_D[1]~input_o\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \bMKR_D[1]~input_o\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~0_combout\);

-- Location: FF_X39_Y17_N27
\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\);

-- Location: LCCOMB_X38_Y15_N20
\TRIV_Schematic_inst|inst|dtm|talu|t3_1\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|talu|t3_1~combout\ = \TRIV_Schematic_inst|inst|dtm|reg|memory[0][65]~q\ $ (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][110]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][65]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][110]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|talu|t3_1~combout\);

-- Location: LCCOMB_X39_Y15_N22
\TRIV_Schematic_inst|inst|dtm|talu|out_reg_1[0]\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|talu|out_reg_1\(0) = \TRIV_Schematic_inst|inst|dtm|talu|t3_1~combout\ $ (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][68]~q\ $ (((\TRIV_Schematic_inst|inst|dtm|reg|memory[0][109]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|reg|memory[0][108]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001011000111100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][109]~q\,
	datab => \TRIV_Schematic_inst|inst|dtm|talu|t3_1~combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][68]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][108]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|talu|out_reg_1\(0));

-- Location: FF_X39_Y17_N13
\TRIV_Schematic_inst|inst|dtm|uc_triv|load\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	asdata => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\);

-- Location: FF_X39_Y15_N23
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|talu|out_reg_1\(0),
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	sclr => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][0]~q\);

-- Location: LCCOMB_X39_Y15_N0
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~91\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~91_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][0]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~91_combout\);

-- Location: FF_X39_Y15_N1
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~91_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][1]~q\);

-- Location: LCCOMB_X39_Y15_N30
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~90\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~90_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][1]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][1]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~90_combout\);

-- Location: FF_X39_Y15_N31
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~90_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][2]~q\);

-- Location: LCCOMB_X39_Y15_N8
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~89\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~89_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][2]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][2]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~89_combout\);

-- Location: FF_X39_Y15_N9
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~89_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][3]~q\);

-- Location: LCCOMB_X39_Y15_N4
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~88\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~88_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][3]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][3]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~88_combout\);

-- Location: FF_X39_Y15_N5
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~88_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][4]~q\);

-- Location: LCCOMB_X39_Y15_N6
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~87\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~87_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][4]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][4]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~87_combout\);

-- Location: FF_X39_Y15_N7
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~87_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][5]~q\);

-- Location: LCCOMB_X39_Y15_N10
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~86\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~86_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][5]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][5]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~86_combout\);

-- Location: FF_X39_Y15_N11
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~86_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][6]~q\);

-- Location: LCCOMB_X39_Y15_N12
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~85\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~85_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][6]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][6]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~85_combout\);

-- Location: FF_X39_Y15_N13
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~85_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][7]~q\);

-- Location: LCCOMB_X39_Y15_N28
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~84\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~84_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][7]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][7]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~84_combout\);

-- Location: FF_X39_Y15_N29
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~84_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][8]~q\);

-- Location: LCCOMB_X39_Y19_N20
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~83\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~83_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][8]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][8]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~83_combout\);

-- Location: FF_X39_Y19_N21
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~83_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][9]~q\);

-- Location: LCCOMB_X39_Y19_N10
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~82\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~82_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][9]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][9]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~82_combout\);

-- Location: FF_X39_Y19_N11
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~82_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][10]~q\);

-- Location: LCCOMB_X39_Y19_N24
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~81\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~81_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][10]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][10]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~81_combout\);

-- Location: FF_X39_Y19_N25
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~81_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][11]~q\);

-- Location: LCCOMB_X39_Y19_N12
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~80\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~80_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][11]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][11]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~80_combout\);

-- Location: FF_X39_Y19_N13
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~80_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][12]~q\);

-- Location: LCCOMB_X39_Y19_N16
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~79\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~79_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][12]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][12]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~79_combout\);

-- Location: FF_X39_Y19_N17
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~79_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][13]~q\);

-- Location: LCCOMB_X39_Y19_N0
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~78\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~78_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][13]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][13]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~78_combout\);

-- Location: FF_X39_Y19_N1
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~78_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][14]~q\);

-- Location: LCCOMB_X39_Y19_N2
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~77\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~77_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][14]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][14]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~77_combout\);

-- Location: FF_X39_Y19_N3
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~77_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][15]~q\);

-- Location: LCCOMB_X39_Y19_N6
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~76\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~76_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][15]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][15]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~76_combout\);

-- Location: FF_X39_Y19_N7
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~76_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][16]~q\);

-- Location: LCCOMB_X39_Y19_N4
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~75\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~75_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][16]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][16]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~75_combout\);

-- Location: FF_X39_Y19_N5
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~75_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][17]~q\);

-- Location: LCCOMB_X39_Y19_N8
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~74\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~74_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][17]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][17]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~74_combout\);

-- Location: FF_X39_Y19_N9
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~74_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][18]~q\);

-- Location: LCCOMB_X39_Y19_N14
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~73\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~73_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][18]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][18]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~73_combout\);

-- Location: FF_X39_Y19_N15
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~73_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][19]~q\);

-- Location: LCCOMB_X39_Y19_N22
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~72\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~72_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][19]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][19]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~72_combout\);

-- Location: FF_X39_Y19_N23
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~72_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][20]~q\);

-- Location: LCCOMB_X39_Y19_N26
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~71\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~71_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][20]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][20]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~71_combout\);

-- Location: FF_X39_Y19_N27
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~71_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][21]~q\);

-- Location: LCCOMB_X39_Y19_N30
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~70\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~70_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][21]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][21]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~70_combout\);

-- Location: FF_X39_Y19_N31
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~70_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][22]~q\);

-- Location: LCCOMB_X39_Y19_N18
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~69\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~69_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][22]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][22]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~69_combout\);

-- Location: FF_X39_Y19_N19
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~69_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][23]~q\);

-- Location: LCCOMB_X39_Y19_N28
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~68\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~68_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][23]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][23]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~68_combout\);

-- Location: FF_X39_Y19_N29
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~68_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][24]~q\);

-- Location: LCCOMB_X38_Y19_N8
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~67\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~67_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][24]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][24]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~67_combout\);

-- Location: FF_X38_Y19_N9
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~67_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][25]~q\);

-- Location: LCCOMB_X38_Y19_N4
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~66\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~66_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][25]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][25]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~66_combout\);

-- Location: FF_X38_Y19_N5
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~66_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][26]~q\);

-- Location: LCCOMB_X38_Y19_N22
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~65\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~65_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][26]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][26]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~65_combout\);

-- Location: FF_X38_Y19_N23
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~65_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][27]~q\);

-- Location: LCCOMB_X38_Y19_N10
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~64\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~64_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][27]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][27]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~64_combout\);

-- Location: FF_X38_Y19_N11
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~64_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][28]~q\);

-- Location: LCCOMB_X38_Y19_N12
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~63\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~63_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][28]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][28]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~63_combout\);

-- Location: FF_X38_Y19_N13
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~63_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][29]~q\);

-- Location: LCCOMB_X38_Y19_N20
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~62\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~62_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][29]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][29]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~62_combout\);

-- Location: FF_X38_Y19_N21
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~62_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][30]~q\);

-- Location: LCCOMB_X38_Y19_N24
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~61\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~61_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][30]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][30]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~61_combout\);

-- Location: FF_X38_Y19_N25
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~61_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][31]~q\);

-- Location: LCCOMB_X38_Y19_N18
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~60\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~60_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][31]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][31]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~60_combout\);

-- Location: FF_X38_Y19_N19
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][32]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~60_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][32]~q\);

-- Location: LCCOMB_X38_Y19_N28
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~59\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~59_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][32]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][32]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~59_combout\);

-- Location: FF_X38_Y19_N29
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][33]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~59_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][33]~q\);

-- Location: LCCOMB_X37_Y19_N14
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~58\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~58_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][33]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][33]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~58_combout\);

-- Location: FF_X37_Y19_N15
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][34]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~58_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][34]~q\);

-- Location: LCCOMB_X37_Y19_N22
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~57\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~57_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][34]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][34]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~57_combout\);

-- Location: FF_X37_Y19_N23
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][35]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~57_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][35]~q\);

-- Location: LCCOMB_X37_Y19_N8
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~56\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~56_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][35]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][35]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~56_combout\);

-- Location: FF_X37_Y19_N9
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][36]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~56_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][36]~q\);

-- Location: LCCOMB_X37_Y19_N4
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~55\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~55_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][36]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][36]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~55_combout\);

-- Location: FF_X37_Y19_N5
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][37]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~55_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][37]~q\);

-- Location: LCCOMB_X37_Y19_N12
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~54\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~54_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][37]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][37]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~54_combout\);

-- Location: FF_X37_Y19_N13
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][38]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~54_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][38]~q\);

-- Location: LCCOMB_X37_Y19_N6
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~53\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~53_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][38]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][38]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~53_combout\);

-- Location: FF_X37_Y19_N7
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][39]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~53_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][39]~q\);

-- Location: LCCOMB_X37_Y19_N10
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~51\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~51_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][39]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][39]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~51_combout\);

-- Location: FF_X37_Y19_N11
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][40]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~51_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][40]~q\);

-- Location: LCCOMB_X37_Y19_N30
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~49\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~49_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][40]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][40]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~49_combout\);

-- Location: FF_X37_Y19_N31
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][41]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~49_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][41]~q\);

-- Location: LCCOMB_X37_Y19_N0
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~47\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~47_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][41]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][41]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~47_combout\);

-- Location: FF_X37_Y19_N1
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][42]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~47_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][42]~q\);

-- Location: LCCOMB_X37_Y19_N2
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~45\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~45_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][42]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][42]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~45_combout\);

-- Location: FF_X37_Y19_N3
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][43]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~45_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][43]~q\);

-- Location: LCCOMB_X37_Y19_N16
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~43\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~43_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][43]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][43]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~43_combout\);

-- Location: FF_X37_Y19_N17
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][44]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~43_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][44]~q\);

-- Location: LCCOMB_X37_Y19_N26
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~41\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~41_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][44]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][44]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~41_combout\);

-- Location: FF_X37_Y19_N27
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][45]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~41_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][45]~q\);

-- Location: LCCOMB_X37_Y19_N20
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~39\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~39_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][45]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][45]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~39_combout\);

-- Location: FF_X37_Y19_N21
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][46]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~39_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][46]~q\);

-- Location: LCCOMB_X37_Y19_N24
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~37\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~37_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][46]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][46]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~37_combout\);

-- Location: FF_X37_Y19_N25
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][47]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~37_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][47]~q\);

-- Location: LCCOMB_X37_Y19_N18
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~35\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~35_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][47]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][47]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~35_combout\);

-- Location: FF_X37_Y19_N19
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][48]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~35_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][48]~q\);

-- Location: LCCOMB_X37_Y19_N28
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~33\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~33_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][48]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][48]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~33_combout\);

-- Location: FF_X37_Y19_N29
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][49]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~33_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][49]~q\);

-- Location: LCCOMB_X36_Y19_N4
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~31\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~31_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][49]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][49]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~31_combout\);

-- Location: FF_X36_Y19_N5
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][50]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~31_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][50]~q\);

-- Location: LCCOMB_X36_Y19_N26
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~29\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~29_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][50]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][50]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~29_combout\);

-- Location: FF_X36_Y19_N27
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][51]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~29_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][51]~q\);

-- Location: LCCOMB_X36_Y19_N22
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~27\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~27_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][51]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][51]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~27_combout\);

-- Location: FF_X36_Y19_N23
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][52]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~27_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][52]~q\);

-- Location: LCCOMB_X36_Y19_N18
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~25\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~25_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][52]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][52]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~25_combout\);

-- Location: FF_X36_Y19_N19
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][53]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~25_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][53]~q\);

-- Location: LCCOMB_X36_Y19_N14
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~23\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~23_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][53]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][53]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~23_combout\);

-- Location: FF_X36_Y19_N15
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][54]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~23_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][54]~q\);

-- Location: LCCOMB_X36_Y19_N12
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~21\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~21_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][54]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][54]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~21_combout\);

-- Location: FF_X36_Y19_N13
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][55]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~21_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][55]~q\);

-- Location: LCCOMB_X36_Y19_N20
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~19\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~19_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][55]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][55]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~19_combout\);

-- Location: FF_X36_Y19_N21
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][56]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~19_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][56]~q\);

-- Location: LCCOMB_X36_Y19_N24
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~17\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~17_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][56]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][56]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~17_combout\);

-- Location: FF_X36_Y19_N25
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][57]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~17_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][57]~q\);

-- Location: LCCOMB_X36_Y19_N28
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~15\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~15_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][57]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][57]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~15_combout\);

-- Location: FF_X36_Y19_N29
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][58]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~15_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][58]~q\);

-- Location: LCCOMB_X36_Y19_N30
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~13\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~13_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][58]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][58]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~13_combout\);

-- Location: FF_X36_Y19_N31
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][59]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~13_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][59]~q\);

-- Location: LCCOMB_X36_Y19_N2
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~11\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~11_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][59]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][59]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~11_combout\);

-- Location: FF_X36_Y19_N3
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][60]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~11_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][60]~q\);

-- Location: LCCOMB_X36_Y19_N8
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~9\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~9_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][60]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][60]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~9_combout\);

-- Location: FF_X36_Y19_N9
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][61]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~9_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][61]~q\);

-- Location: LCCOMB_X36_Y19_N0
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~7\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~7_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][61]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][61]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~7_combout\);

-- Location: FF_X36_Y19_N1
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][62]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~7_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][62]~q\);

-- Location: LCCOMB_X36_Y19_N6
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~5\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~5_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][62]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][62]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~5_combout\);

-- Location: FF_X36_Y19_N7
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][63]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~5_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][63]~q\);

-- Location: LCCOMB_X36_Y19_N10
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~3\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~3_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][63]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][63]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~3_combout\);

-- Location: FF_X36_Y19_N11
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][64]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~3_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][64]~q\);

-- Location: LCCOMB_X36_Y19_N16
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~1\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~1_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][64]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][64]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~1_combout\);

-- Location: FF_X36_Y19_N17
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][65]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~1_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][65]~q\);

-- Location: LCCOMB_X36_Y15_N12
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~52\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~52_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][65]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][65]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~52_combout\);

-- Location: FF_X36_Y15_N13
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][66]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~52_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][66]~q\);

-- Location: LCCOMB_X36_Y15_N18
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~50\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~50_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][66]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][66]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~50_combout\);

-- Location: FF_X36_Y15_N19
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][67]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~50_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][67]~q\);

-- Location: LCCOMB_X36_Y15_N20
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~48\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~48_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][67]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][67]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~48_combout\);

-- Location: FF_X36_Y15_N21
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][68]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~48_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][68]~q\);

-- Location: LCCOMB_X39_Y15_N2
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~46\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~46_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][68]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][68]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~46_combout\);

-- Location: FF_X39_Y15_N3
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][69]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~46_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][69]~q\);

-- Location: LCCOMB_X39_Y15_N16
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~44\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~44_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][69]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][69]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~44_combout\);

-- Location: FF_X39_Y15_N17
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][70]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~44_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][70]~q\);

-- Location: LCCOMB_X39_Y15_N20
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~42\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~42_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][70]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][70]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~42_combout\);

-- Location: FF_X39_Y15_N21
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][71]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~42_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][71]~q\);

-- Location: LCCOMB_X39_Y15_N24
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~40\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~40_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][71]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][71]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~40_combout\);

-- Location: FF_X39_Y15_N25
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][72]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~40_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][72]~q\);

-- Location: LCCOMB_X39_Y15_N14
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~38\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~38_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][72]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][72]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~38_combout\);

-- Location: FF_X39_Y15_N15
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][73]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~38_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][73]~q\);

-- Location: LCCOMB_X40_Y15_N10
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~36\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~36_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][73]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][73]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~36_combout\);

-- Location: FF_X40_Y15_N11
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][74]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~36_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][74]~q\);

-- Location: LCCOMB_X40_Y15_N24
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~34\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~34_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][74]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][74]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~34_combout\);

-- Location: FF_X40_Y15_N25
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][75]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~34_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][75]~q\);

-- Location: LCCOMB_X40_Y15_N12
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~32\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~32_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][75]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][75]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~32_combout\);

-- Location: FF_X40_Y15_N13
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][76]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~32_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][76]~q\);

-- Location: LCCOMB_X40_Y15_N0
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~30\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~30_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][76]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][76]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~30_combout\);

-- Location: FF_X40_Y15_N1
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][77]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~30_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][77]~q\);

-- Location: LCCOMB_X40_Y15_N28
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~28\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~28_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][77]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][77]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~28_combout\);

-- Location: FF_X40_Y15_N29
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][78]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~28_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][78]~q\);

-- Location: LCCOMB_X40_Y15_N2
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~26\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~26_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][78]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][78]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~26_combout\);

-- Location: FF_X40_Y15_N3
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][79]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~26_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][79]~q\);

-- Location: LCCOMB_X40_Y15_N6
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~24\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~24_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][79]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][79]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~24_combout\);

-- Location: FF_X40_Y15_N7
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][80]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~24_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][80]~q\);

-- Location: LCCOMB_X40_Y15_N16
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~22\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~22_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][80]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][80]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~22_combout\);

-- Location: FF_X40_Y15_N17
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][81]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~22_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][81]~q\);

-- Location: LCCOMB_X40_Y15_N20
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~20\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~20_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][81]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][81]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~20_combout\);

-- Location: FF_X40_Y15_N21
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][82]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~20_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][82]~q\);

-- Location: LCCOMB_X40_Y15_N4
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~18\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~18_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][82]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][82]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~18_combout\);

-- Location: FF_X40_Y15_N5
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][83]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~18_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][83]~q\);

-- Location: LCCOMB_X40_Y15_N14
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~16\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~16_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][83]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][83]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~16_combout\);

-- Location: FF_X40_Y15_N15
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][84]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~16_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][84]~q\);

-- Location: LCCOMB_X40_Y15_N22
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~14\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~14_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][84]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][84]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~14_combout\);

-- Location: FF_X40_Y15_N23
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][85]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~14_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][85]~q\);

-- Location: LCCOMB_X40_Y15_N26
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~12\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~12_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][85]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][85]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~12_combout\);

-- Location: FF_X40_Y15_N27
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][86]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~12_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][86]~q\);

-- Location: LCCOMB_X40_Y15_N30
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~10\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~10_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][86]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][86]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~10_combout\);

-- Location: FF_X40_Y15_N31
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][87]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~10_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][87]~q\);

-- Location: LCCOMB_X40_Y15_N18
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~8\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~8_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][87]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][87]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~8_combout\);

-- Location: FF_X40_Y15_N19
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][88]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~8_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][88]~q\);

-- Location: LCCOMB_X40_Y15_N8
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~6\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~6_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][88]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][88]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~6_combout\);

-- Location: FF_X40_Y15_N9
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][89]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~6_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][89]~q\);

-- Location: LCCOMB_X36_Y15_N2
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~4\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~4_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][89]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][89]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~4_combout\);

-- Location: FF_X36_Y15_N3
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][90]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~4_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][90]~q\);

-- Location: LCCOMB_X36_Y15_N26
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~2\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~2_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][90]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][90]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~2_combout\);

-- Location: FF_X36_Y15_N27
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][91]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~2_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][91]~q\);

-- Location: LCCOMB_X36_Y15_N8
\TRIV_Schematic_inst|inst|dtm|reg_key|memory~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory~0_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][91]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][91]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~0_combout\);

-- Location: FF_X36_Y15_N9
\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][92]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_key|memory~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][92]~q\);

-- Location: LCCOMB_X36_Y15_N0
\TRIV_Schematic_inst|inst|dtm|talu|t1_1\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|talu|t1_1~combout\ = \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][92]~q\ $ (\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][65]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][92]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][65]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|talu|t1_1~combout\);

-- Location: LCCOMB_X36_Y15_N22
\TRIV_Schematic_inst|inst|dtm|talu|out_reg_2[0]\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|talu|out_reg_2\(0) = \TRIV_Schematic_inst|inst|dtm|talu|t1_1~combout\ $ (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][77]~q\ $ (((\TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][90]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][91]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001010101101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|talu|t1_1~combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][90]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_key|memory[0][91]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][77]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|talu|out_reg_2\(0));

-- Location: FF_X36_Y15_N23
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|talu|out_reg_2\(0),
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	sclr => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][0]~q\);

-- Location: LCCOMB_X36_Y15_N10
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~82\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~82_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][0]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~82_combout\);

-- Location: FF_X36_Y15_N11
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~82_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][1]~q\);

-- Location: LCCOMB_X36_Y15_N30
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~81\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~81_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][1]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][1]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~81_combout\);

-- Location: FF_X36_Y15_N31
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~81_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][2]~q\);

-- Location: LCCOMB_X36_Y15_N28
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~80\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~80_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][2]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][2]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~80_combout\);

-- Location: FF_X36_Y15_N29
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~80_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][3]~q\);

-- Location: LCCOMB_X36_Y15_N24
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~79\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~79_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][3]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][3]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~79_combout\);

-- Location: FF_X36_Y15_N25
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~79_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][4]~q\);

-- Location: LCCOMB_X36_Y18_N18
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~78\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~78_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][4]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][4]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~78_combout\);

-- Location: FF_X36_Y18_N19
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~78_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][5]~q\);

-- Location: LCCOMB_X36_Y18_N28
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~77\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~77_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][5]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][5]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~77_combout\);

-- Location: FF_X36_Y18_N29
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~77_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][6]~q\);

-- Location: LCCOMB_X36_Y18_N6
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~76\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~76_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][6]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][6]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~76_combout\);

-- Location: FF_X36_Y18_N7
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~76_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][7]~q\);

-- Location: LCCOMB_X36_Y18_N8
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~75\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~75_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][7]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][7]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~75_combout\);

-- Location: FF_X36_Y18_N9
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~75_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][8]~q\);

-- Location: LCCOMB_X36_Y18_N20
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~74\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~74_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][8]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][8]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~74_combout\);

-- Location: FF_X36_Y18_N21
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~74_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][9]~q\);

-- Location: LCCOMB_X36_Y18_N26
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~73\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~73_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][9]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][9]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~73_combout\);

-- Location: FF_X36_Y18_N27
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~73_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][10]~q\);

-- Location: LCCOMB_X36_Y18_N10
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~72\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~72_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][10]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][10]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~72_combout\);

-- Location: FF_X36_Y18_N11
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~72_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][11]~q\);

-- Location: LCCOMB_X36_Y18_N30
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~71\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~71_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][11]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][11]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~71_combout\);

-- Location: FF_X36_Y18_N31
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~71_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][12]~q\);

-- Location: LCCOMB_X36_Y18_N16
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~70\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~70_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][12]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][12]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~70_combout\);

-- Location: FF_X36_Y18_N17
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~70_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][13]~q\);

-- Location: LCCOMB_X36_Y18_N12
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~69\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~69_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][13]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][13]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~69_combout\);

-- Location: FF_X36_Y18_N13
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~69_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][14]~q\);

-- Location: LCCOMB_X36_Y18_N4
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~68\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~68_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][14]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][14]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~68_combout\);

-- Location: FF_X36_Y18_N5
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~68_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][15]~q\);

-- Location: LCCOMB_X36_Y18_N22
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~67\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~67_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][15]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][15]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~67_combout\);

-- Location: FF_X36_Y18_N23
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~67_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][16]~q\);

-- Location: LCCOMB_X36_Y18_N0
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~66\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~66_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][16]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][16]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~66_combout\);

-- Location: FF_X36_Y18_N1
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~66_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][17]~q\);

-- Location: LCCOMB_X36_Y18_N24
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~65\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~65_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][17]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][17]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~65_combout\);

-- Location: FF_X36_Y18_N25
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~65_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][18]~q\);

-- Location: LCCOMB_X36_Y18_N2
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~64\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~64_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][18]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][18]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~64_combout\);

-- Location: FF_X36_Y18_N3
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~64_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][19]~q\);

-- Location: LCCOMB_X36_Y18_N14
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~63\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~63_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][19]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][19]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~63_combout\);

-- Location: FF_X36_Y18_N15
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~63_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][20]~q\);

-- Location: LCCOMB_X37_Y18_N30
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~62\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~62_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][20]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][20]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~62_combout\);

-- Location: FF_X37_Y18_N31
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~62_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][21]~q\);

-- Location: LCCOMB_X37_Y18_N4
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~61\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~61_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][21]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][21]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~61_combout\);

-- Location: FF_X37_Y18_N5
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~61_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][22]~q\);

-- Location: LCCOMB_X37_Y18_N22
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~60\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~60_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][22]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][22]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~60_combout\);

-- Location: FF_X37_Y18_N23
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~60_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][23]~q\);

-- Location: LCCOMB_X37_Y18_N24
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~59\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~59_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][23]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][23]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~59_combout\);

-- Location: FF_X37_Y18_N25
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~59_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][24]~q\);

-- Location: LCCOMB_X37_Y18_N28
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~58\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~58_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][24]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][24]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~58_combout\);

-- Location: FF_X37_Y18_N29
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~58_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][25]~q\);

-- Location: LCCOMB_X37_Y18_N18
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~57\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~57_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][25]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][25]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~57_combout\);

-- Location: FF_X37_Y18_N19
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~57_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][26]~q\);

-- Location: LCCOMB_X37_Y18_N8
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~56\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~56_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][26]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][26]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~56_combout\);

-- Location: FF_X37_Y18_N9
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~56_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][27]~q\);

-- Location: LCCOMB_X37_Y18_N2
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~55\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~55_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][27]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][27]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~55_combout\);

-- Location: FF_X37_Y18_N3
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~55_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][28]~q\);

-- Location: LCCOMB_X37_Y18_N20
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~54\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~54_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][28]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][28]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~54_combout\);

-- Location: FF_X37_Y18_N21
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~54_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][29]~q\);

-- Location: LCCOMB_X37_Y18_N16
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~53\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~53_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][29]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][29]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~53_combout\);

-- Location: FF_X37_Y18_N17
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~53_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][30]~q\);

-- Location: LCCOMB_X37_Y18_N6
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~52\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~52_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][30]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][30]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~52_combout\);

-- Location: FF_X37_Y18_N7
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~52_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][31]~q\);

-- Location: LCCOMB_X37_Y18_N12
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~51\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~51_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][31]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][31]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~51_combout\);

-- Location: FF_X37_Y18_N13
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][32]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~51_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][32]~q\);

-- Location: LCCOMB_X37_Y18_N26
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~50\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~50_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][32]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][32]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~50_combout\);

-- Location: FF_X37_Y18_N27
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][33]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~50_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][33]~q\);

-- Location: LCCOMB_X37_Y18_N0
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~49\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~49_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][33]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][33]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~49_combout\);

-- Location: FF_X37_Y18_N1
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][34]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~49_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][34]~q\);

-- Location: LCCOMB_X37_Y18_N10
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~48\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~48_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][34]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][34]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~48_combout\);

-- Location: FF_X37_Y18_N11
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][35]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~48_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][35]~q\);

-- Location: LCCOMB_X37_Y18_N14
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~47\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~47_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][35]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][35]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~47_combout\);

-- Location: FF_X37_Y18_N15
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][36]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~47_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][36]~q\);

-- Location: LCCOMB_X38_Y18_N18
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~46\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~46_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][36]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][36]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~46_combout\);

-- Location: FF_X38_Y18_N19
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][37]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~46_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][37]~q\);

-- Location: LCCOMB_X38_Y18_N26
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~45\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~45_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][37]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][37]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~45_combout\);

-- Location: FF_X38_Y18_N27
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][38]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~45_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][38]~q\);

-- Location: LCCOMB_X38_Y18_N4
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~44\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~44_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][38]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][38]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~44_combout\);

-- Location: FF_X38_Y18_N5
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][39]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~44_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][39]~q\);

-- Location: LCCOMB_X38_Y18_N8
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~43\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~43_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][39]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][39]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~43_combout\);

-- Location: FF_X38_Y18_N9
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][40]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~43_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][40]~q\);

-- Location: LCCOMB_X38_Y18_N30
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~42\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~42_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][40]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][40]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~42_combout\);

-- Location: FF_X38_Y18_N31
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][41]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~42_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][41]~q\);

-- Location: LCCOMB_X38_Y18_N22
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~41\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~41_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][41]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][41]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~41_combout\);

-- Location: FF_X38_Y18_N23
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][42]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~41_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][42]~q\);

-- Location: LCCOMB_X38_Y18_N10
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~40\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~40_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][42]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][42]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~40_combout\);

-- Location: FF_X38_Y18_N11
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][43]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~40_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][43]~q\);

-- Location: LCCOMB_X38_Y18_N12
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~39\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~39_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][43]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][43]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~39_combout\);

-- Location: FF_X38_Y18_N13
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][44]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~39_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][44]~q\);

-- Location: LCCOMB_X38_Y18_N16
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~38\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~38_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][44]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][44]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~38_combout\);

-- Location: FF_X38_Y18_N17
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][45]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~38_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][45]~q\);

-- Location: LCCOMB_X38_Y18_N20
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~37\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~37_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][45]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][45]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~37_combout\);

-- Location: FF_X38_Y18_N21
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][46]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~37_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][46]~q\);

-- Location: LCCOMB_X38_Y18_N24
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~36\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~36_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][46]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][46]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~36_combout\);

-- Location: FF_X38_Y18_N25
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][47]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~36_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][47]~q\);

-- Location: LCCOMB_X38_Y18_N28
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~35\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~35_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][47]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][47]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~35_combout\);

-- Location: FF_X38_Y18_N29
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][48]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~35_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][48]~q\);

-- Location: LCCOMB_X38_Y18_N14
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~34\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~34_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][48]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][48]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~34_combout\);

-- Location: FF_X38_Y18_N15
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][49]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~34_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][49]~q\);

-- Location: LCCOMB_X39_Y18_N10
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~33\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~33_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][49]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][49]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~33_combout\);

-- Location: FF_X39_Y18_N11
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][50]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~33_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][50]~q\);

-- Location: LCCOMB_X39_Y18_N24
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~32\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~32_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][50]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][50]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~32_combout\);

-- Location: FF_X39_Y18_N25
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][51]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~32_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][51]~q\);

-- Location: LCCOMB_X39_Y18_N12
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~31\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~31_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][51]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][51]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~31_combout\);

-- Location: FF_X39_Y18_N13
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][52]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~31_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][52]~q\);

-- Location: LCCOMB_X39_Y18_N0
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~30\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~30_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][52]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][52]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~30_combout\);

-- Location: FF_X39_Y18_N1
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][53]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~30_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][53]~q\);

-- Location: LCCOMB_X39_Y18_N28
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~29\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~29_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][53]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][53]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~29_combout\);

-- Location: FF_X39_Y18_N29
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][54]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~29_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][54]~q\);

-- Location: LCCOMB_X39_Y18_N2
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~27\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~27_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][54]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][54]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~27_combout\);

-- Location: FF_X39_Y18_N3
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][55]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~27_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][55]~q\);

-- Location: LCCOMB_X39_Y18_N6
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~25\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~25_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][55]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][55]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~25_combout\);

-- Location: FF_X39_Y18_N7
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][56]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~25_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][56]~q\);

-- Location: LCCOMB_X39_Y18_N20
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~23\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~23_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][56]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][56]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~23_combout\);

-- Location: FF_X39_Y18_N21
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][57]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~23_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][57]~q\);

-- Location: LCCOMB_X39_Y18_N30
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~21\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~21_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][57]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][57]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~21_combout\);

-- Location: FF_X39_Y18_N31
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][58]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~21_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][58]~q\);

-- Location: LCCOMB_X39_Y18_N22
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~19\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~19_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][58]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][58]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~19_combout\);

-- Location: FF_X39_Y18_N23
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][59]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~19_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][59]~q\);

-- Location: LCCOMB_X39_Y18_N26
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~17\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~17_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][59]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][59]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~17_combout\);

-- Location: FF_X39_Y18_N27
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][60]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~17_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][60]~q\);

-- Location: LCCOMB_X39_Y18_N4
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~15\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~15_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][60]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][60]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~15_combout\);

-- Location: FF_X39_Y18_N5
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][61]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~15_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][61]~q\);

-- Location: LCCOMB_X39_Y18_N8
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~13\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~13_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][61]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][61]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~13_combout\);

-- Location: FF_X39_Y18_N9
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][62]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~13_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][62]~q\);

-- Location: LCCOMB_X39_Y18_N14
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~11\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~11_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][62]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][62]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~11_combout\);

-- Location: FF_X39_Y18_N15
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][63]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~11_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][63]~q\);

-- Location: LCCOMB_X39_Y18_N18
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~9\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~9_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][63]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][63]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~9_combout\);

-- Location: FF_X39_Y18_N19
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][64]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~9_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][64]~q\);

-- Location: LCCOMB_X39_Y18_N16
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~7\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~7_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][64]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][64]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~7_combout\);

-- Location: FF_X39_Y18_N17
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][65]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~7_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][65]~q\);

-- Location: LCCOMB_X39_Y15_N18
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~5\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~5_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][65]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][65]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~5_combout\);

-- Location: FF_X39_Y15_N19
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][66]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~5_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][66]~q\);

-- Location: LCCOMB_X39_Y15_N26
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~3\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~3_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][66]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][66]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~3_combout\);

-- Location: FF_X39_Y15_N27
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][67]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~3_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][67]~q\);

-- Location: LCCOMB_X38_Y15_N10
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~1\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~1_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][67]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][67]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~1_combout\);

-- Location: FF_X38_Y15_N11
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][68]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~1_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][68]~q\);

-- Location: LCCOMB_X38_Y15_N22
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~28\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~28_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][68]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][68]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~28_combout\);

-- Location: FF_X38_Y15_N23
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][69]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~28_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][69]~q\);

-- Location: LCCOMB_X38_Y15_N30
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~26\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~26_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][69]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][69]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~26_combout\);

-- Location: FF_X38_Y15_N31
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][70]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~26_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][70]~q\);

-- Location: LCCOMB_X38_Y15_N18
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~24\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~24_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][70]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][70]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~24_combout\);

-- Location: FF_X38_Y15_N19
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][71]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~24_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][71]~q\);

-- Location: LCCOMB_X37_Y15_N10
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~22\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~22_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][71]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][71]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~22_combout\);

-- Location: FF_X37_Y15_N11
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][72]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~22_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][72]~q\);

-- Location: LCCOMB_X37_Y15_N0
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~20\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~20_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][72]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][72]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~20_combout\);

-- Location: FF_X37_Y15_N1
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][73]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~20_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][73]~q\);

-- Location: LCCOMB_X37_Y15_N2
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~18\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~18_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][73]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][73]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~18_combout\);

-- Location: FF_X37_Y15_N3
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][74]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~18_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][74]~q\);

-- Location: LCCOMB_X37_Y15_N26
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~16\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~16_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][74]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][74]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~16_combout\);

-- Location: FF_X37_Y15_N27
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][75]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~16_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][75]~q\);

-- Location: LCCOMB_X36_Y15_N4
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~14\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~14_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][75]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][75]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~14_combout\);

-- Location: FF_X36_Y15_N5
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][76]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~14_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][76]~q\);

-- Location: LCCOMB_X36_Y15_N16
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~12\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~12_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][76]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][76]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~12_combout\);

-- Location: FF_X36_Y15_N17
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][77]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~12_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][77]~q\);

-- Location: LCCOMB_X36_Y15_N6
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~10\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~10_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][77]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][77]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~10_combout\);

-- Location: FF_X36_Y15_N7
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][78]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~10_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][78]~q\);

-- Location: LCCOMB_X36_Y15_N14
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~8\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~8_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][78]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][78]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~8_combout\);

-- Location: FF_X36_Y15_N15
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][79]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~8_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][79]~q\);

-- Location: LCCOMB_X37_Y15_N16
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~6\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~6_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][79]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][79]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~6_combout\);

-- Location: FF_X37_Y15_N17
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][80]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~6_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][80]~q\);

-- Location: LCCOMB_X37_Y15_N18
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~4\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~4_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][80]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][80]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~4_combout\);

-- Location: FF_X37_Y15_N19
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][81]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~4_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][81]~q\);

-- Location: LCCOMB_X37_Y15_N12
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~2\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~2_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][81]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][81]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~2_combout\);

-- Location: FF_X37_Y15_N13
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][82]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~2_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][82]~q\);

-- Location: LCCOMB_X38_Y15_N26
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][82]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][82]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~0_combout\);

-- Location: FF_X38_Y15_N27
\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][83]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][83]~q\);

-- Location: LCCOMB_X38_Y15_N24
\TRIV_Schematic_inst|inst|dtm|talu|t2_1\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|talu|t2_1~combout\ = \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][83]~q\ $ (\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][68]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][83]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][68]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|talu|t2_1~combout\);

-- Location: LCCOMB_X37_Y15_N4
\TRIV_Schematic_inst|inst|dtm|talu|out_reg_3[0]\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|talu|out_reg_3\(0) = \TRIV_Schematic_inst|inst|dtm|reg|memory[0][86]~q\ $ (\TRIV_Schematic_inst|inst|dtm|talu|t2_1~combout\ $ (((\TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][81]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][82]~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001010101101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][86]~q\,
	datab => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][81]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg_iv|memory[0][82]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|talu|t2_1~combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|talu|out_reg_3\(0));

-- Location: FF_X37_Y15_N5
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|talu|out_reg_3\(0),
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	sclr => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][0]~q\);

-- Location: LCCOMB_X37_Y15_N22
\TRIV_Schematic_inst|inst|dtm|reg|memory~109\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~109_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][0]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~109_combout\);

-- Location: FF_X37_Y15_N23
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~109_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][1]~q\);

-- Location: LCCOMB_X37_Y15_N20
\TRIV_Schematic_inst|inst|dtm|reg|memory~108\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~108_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][1]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][1]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~108_combout\);

-- Location: FF_X37_Y15_N21
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~108_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][2]~q\);

-- Location: LCCOMB_X37_Y15_N30
\TRIV_Schematic_inst|inst|dtm|reg|memory~107\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~107_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][2]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][2]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~107_combout\);

-- Location: FF_X37_Y15_N31
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~107_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][3]~q\);

-- Location: LCCOMB_X37_Y15_N8
\TRIV_Schematic_inst|inst|dtm|reg|memory~106\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~106_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][3]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][3]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~106_combout\);

-- Location: FF_X37_Y15_N9
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~106_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][4]~q\);

-- Location: LCCOMB_X37_Y15_N24
\TRIV_Schematic_inst|inst|dtm|reg|memory~105\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~105_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][4]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][4]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~105_combout\);

-- Location: FF_X37_Y15_N25
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~105_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][5]~q\);

-- Location: LCCOMB_X37_Y17_N8
\TRIV_Schematic_inst|inst|dtm|reg|memory~104\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~104_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][5]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][5]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~104_combout\);

-- Location: FF_X37_Y17_N9
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~104_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][6]~q\);

-- Location: LCCOMB_X37_Y17_N10
\TRIV_Schematic_inst|inst|dtm|reg|memory~103\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~103_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][6]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][6]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~103_combout\);

-- Location: FF_X37_Y17_N11
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~103_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][7]~q\);

-- Location: LCCOMB_X37_Y17_N28
\TRIV_Schematic_inst|inst|dtm|reg|memory~102\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~102_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][7]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][7]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~102_combout\);

-- Location: FF_X37_Y17_N29
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~102_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][8]~q\);

-- Location: LCCOMB_X36_Y17_N20
\TRIV_Schematic_inst|inst|dtm|reg|memory~101\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~101_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][8]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][8]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~101_combout\);

-- Location: FF_X36_Y17_N21
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~101_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][9]~q\);

-- Location: LCCOMB_X36_Y17_N0
\TRIV_Schematic_inst|inst|dtm|reg|memory~100\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~100_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][9]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][9]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~100_combout\);

-- Location: FF_X36_Y17_N1
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~100_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][10]~q\);

-- Location: LCCOMB_X36_Y17_N24
\TRIV_Schematic_inst|inst|dtm|reg|memory~99\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~99_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][10]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][10]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~99_combout\);

-- Location: FF_X36_Y17_N25
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~99_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][11]~q\);

-- Location: LCCOMB_X36_Y17_N18
\TRIV_Schematic_inst|inst|dtm|reg|memory~98\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~98_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][11]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][11]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~98_combout\);

-- Location: FF_X36_Y17_N19
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~98_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][12]~q\);

-- Location: LCCOMB_X36_Y17_N14
\TRIV_Schematic_inst|inst|dtm|reg|memory~97\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~97_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][12]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][12]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~97_combout\);

-- Location: FF_X36_Y17_N15
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~97_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][13]~q\);

-- Location: LCCOMB_X36_Y17_N8
\TRIV_Schematic_inst|inst|dtm|reg|memory~96\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~96_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][13]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][13]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~96_combout\);

-- Location: FF_X36_Y17_N9
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~96_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][14]~q\);

-- Location: LCCOMB_X36_Y17_N6
\TRIV_Schematic_inst|inst|dtm|reg|memory~95\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~95_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][14]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][14]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~95_combout\);

-- Location: FF_X36_Y17_N7
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~95_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][15]~q\);

-- Location: LCCOMB_X36_Y17_N2
\TRIV_Schematic_inst|inst|dtm|reg|memory~94\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~94_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][15]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][15]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~94_combout\);

-- Location: FF_X36_Y17_N3
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~94_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][16]~q\);

-- Location: LCCOMB_X36_Y17_N16
\TRIV_Schematic_inst|inst|dtm|reg|memory~93\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~93_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][16]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][16]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~93_combout\);

-- Location: FF_X36_Y17_N17
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~93_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][17]~q\);

-- Location: LCCOMB_X36_Y17_N10
\TRIV_Schematic_inst|inst|dtm|reg|memory~92\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~92_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][17]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][17]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~92_combout\);

-- Location: FF_X36_Y17_N11
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~92_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][18]~q\);

-- Location: LCCOMB_X36_Y17_N26
\TRIV_Schematic_inst|inst|dtm|reg|memory~91\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~91_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][18]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][18]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~91_combout\);

-- Location: FF_X36_Y17_N27
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~91_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][19]~q\);

-- Location: LCCOMB_X36_Y17_N4
\TRIV_Schematic_inst|inst|dtm|reg|memory~90\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~90_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][19]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][19]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~90_combout\);

-- Location: FF_X36_Y17_N5
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~90_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][20]~q\);

-- Location: LCCOMB_X36_Y17_N28
\TRIV_Schematic_inst|inst|dtm|reg|memory~89\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~89_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][20]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][20]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~89_combout\);

-- Location: FF_X36_Y17_N29
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~89_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][21]~q\);

-- Location: LCCOMB_X36_Y17_N12
\TRIV_Schematic_inst|inst|dtm|reg|memory~87\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~87_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][21]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][21]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~87_combout\);

-- Location: FF_X36_Y17_N13
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~87_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][22]~q\);

-- Location: LCCOMB_X36_Y17_N22
\TRIV_Schematic_inst|inst|dtm|reg|memory~85\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~85_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][22]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][22]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~85_combout\);

-- Location: FF_X36_Y17_N23
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~85_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][23]~q\);

-- Location: LCCOMB_X36_Y17_N30
\TRIV_Schematic_inst|inst|dtm|reg|memory~83\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~83_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][23]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][23]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~83_combout\);

-- Location: FF_X36_Y17_N31
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~83_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][24]~q\);

-- Location: LCCOMB_X36_Y16_N22
\TRIV_Schematic_inst|inst|dtm|reg|memory~81\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~81_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][24]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][24]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~81_combout\);

-- Location: FF_X36_Y16_N23
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~81_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][25]~q\);

-- Location: LCCOMB_X36_Y16_N18
\TRIV_Schematic_inst|inst|dtm|reg|memory~79\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~79_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][25]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][25]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~79_combout\);

-- Location: FF_X36_Y16_N19
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~79_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][26]~q\);

-- Location: LCCOMB_X36_Y16_N2
\TRIV_Schematic_inst|inst|dtm|reg|memory~77\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~77_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][26]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][26]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~77_combout\);

-- Location: FF_X36_Y16_N3
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~77_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][27]~q\);

-- Location: LCCOMB_X36_Y16_N0
\TRIV_Schematic_inst|inst|dtm|reg|memory~75\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~75_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][27]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][27]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~75_combout\);

-- Location: FF_X36_Y16_N1
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~75_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][28]~q\);

-- Location: LCCOMB_X36_Y16_N6
\TRIV_Schematic_inst|inst|dtm|reg|memory~73\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~73_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][28]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][28]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~73_combout\);

-- Location: FF_X36_Y16_N7
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~73_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][29]~q\);

-- Location: LCCOMB_X36_Y16_N16
\TRIV_Schematic_inst|inst|dtm|reg|memory~71\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~71_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][29]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][29]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~71_combout\);

-- Location: FF_X36_Y16_N17
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~71_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][30]~q\);

-- Location: LCCOMB_X36_Y16_N8
\TRIV_Schematic_inst|inst|dtm|reg|memory~69\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~69_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][30]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][30]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~69_combout\);

-- Location: FF_X36_Y16_N9
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~69_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][31]~q\);

-- Location: LCCOMB_X36_Y16_N30
\TRIV_Schematic_inst|inst|dtm|reg|memory~67\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~67_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][31]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][31]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~67_combout\);

-- Location: FF_X36_Y16_N31
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][32]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~67_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][32]~q\);

-- Location: LCCOMB_X36_Y16_N14
\TRIV_Schematic_inst|inst|dtm|reg|memory~65\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~65_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][32]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][32]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~65_combout\);

-- Location: FF_X36_Y16_N15
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][33]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~65_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][33]~q\);

-- Location: LCCOMB_X36_Y16_N20
\TRIV_Schematic_inst|inst|dtm|reg|memory~63\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~63_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][33]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][33]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~63_combout\);

-- Location: FF_X36_Y16_N21
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][34]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~63_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][34]~q\);

-- Location: LCCOMB_X36_Y16_N12
\TRIV_Schematic_inst|inst|dtm|reg|memory~61\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~61_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][34]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][34]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~61_combout\);

-- Location: FF_X36_Y16_N13
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][35]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~61_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][35]~q\);

-- Location: LCCOMB_X36_Y16_N10
\TRIV_Schematic_inst|inst|dtm|reg|memory~59\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~59_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][35]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][35]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~59_combout\);

-- Location: FF_X36_Y16_N11
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][36]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~59_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][36]~q\);

-- Location: LCCOMB_X36_Y16_N24
\TRIV_Schematic_inst|inst|dtm|reg|memory~57\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~57_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][36]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][36]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~57_combout\);

-- Location: FF_X36_Y16_N25
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][37]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~57_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][37]~q\);

-- Location: LCCOMB_X36_Y16_N26
\TRIV_Schematic_inst|inst|dtm|reg|memory~55\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~55_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][37]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][37]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~55_combout\);

-- Location: FF_X36_Y16_N27
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][38]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~55_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][38]~q\);

-- Location: LCCOMB_X36_Y16_N28
\TRIV_Schematic_inst|inst|dtm|reg|memory~53\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~53_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][38]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][38]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~53_combout\);

-- Location: FF_X36_Y16_N29
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][39]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~53_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][39]~q\);

-- Location: LCCOMB_X36_Y16_N4
\TRIV_Schematic_inst|inst|dtm|reg|memory~51\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~51_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][39]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][39]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~51_combout\);

-- Location: FF_X36_Y16_N5
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][40]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~51_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][40]~q\);

-- Location: LCCOMB_X36_Y14_N22
\TRIV_Schematic_inst|inst|dtm|reg|memory~49\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~49_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][40]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][40]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~49_combout\);

-- Location: FF_X36_Y14_N23
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][41]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~49_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][41]~q\);

-- Location: LCCOMB_X36_Y14_N10
\TRIV_Schematic_inst|inst|dtm|reg|memory~47\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~47_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][41]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][41]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~47_combout\);

-- Location: FF_X36_Y14_N11
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][42]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~47_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][42]~q\);

-- Location: LCCOMB_X36_Y14_N4
\TRIV_Schematic_inst|inst|dtm|reg|memory~45\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~45_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][42]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][42]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~45_combout\);

-- Location: FF_X36_Y14_N5
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][43]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~45_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][43]~q\);

-- Location: LCCOMB_X36_Y14_N24
\TRIV_Schematic_inst|inst|dtm|reg|memory~43\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~43_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][43]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][43]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~43_combout\);

-- Location: FF_X36_Y14_N25
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][44]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~43_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][44]~q\);

-- Location: LCCOMB_X36_Y14_N2
\TRIV_Schematic_inst|inst|dtm|reg|memory~41\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~41_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][44]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][44]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~41_combout\);

-- Location: FF_X36_Y14_N3
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][45]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~41_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][45]~q\);

-- Location: LCCOMB_X36_Y14_N26
\TRIV_Schematic_inst|inst|dtm|reg|memory~39\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~39_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][45]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][45]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~39_combout\);

-- Location: FF_X36_Y14_N27
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][46]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~39_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][46]~q\);

-- Location: LCCOMB_X36_Y14_N0
\TRIV_Schematic_inst|inst|dtm|reg|memory~37\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~37_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][46]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][46]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~37_combout\);

-- Location: FF_X36_Y14_N1
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][47]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~37_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][47]~q\);

-- Location: LCCOMB_X36_Y14_N6
\TRIV_Schematic_inst|inst|dtm|reg|memory~35\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~35_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][47]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][47]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~35_combout\);

-- Location: FF_X36_Y14_N7
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][48]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~35_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][48]~q\);

-- Location: LCCOMB_X36_Y14_N12
\TRIV_Schematic_inst|inst|dtm|reg|memory~33\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~33_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][48]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][48]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~33_combout\);

-- Location: FF_X36_Y14_N13
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][49]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~33_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][49]~q\);

-- Location: LCCOMB_X36_Y14_N20
\TRIV_Schematic_inst|inst|dtm|reg|memory~31\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~31_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][49]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][49]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~31_combout\);

-- Location: FF_X36_Y14_N21
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][50]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~31_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][50]~q\);

-- Location: LCCOMB_X36_Y14_N28
\TRIV_Schematic_inst|inst|dtm|reg|memory~29\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~29_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][50]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][50]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~29_combout\);

-- Location: FF_X36_Y14_N29
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][51]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~29_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][51]~q\);

-- Location: LCCOMB_X36_Y14_N8
\TRIV_Schematic_inst|inst|dtm|reg|memory~27\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~27_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][51]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][51]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~27_combout\);

-- Location: FF_X36_Y14_N9
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][52]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~27_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][52]~q\);

-- Location: LCCOMB_X36_Y14_N16
\TRIV_Schematic_inst|inst|dtm|reg|memory~25\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~25_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][52]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][52]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~25_combout\);

-- Location: FF_X36_Y14_N17
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][53]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~25_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][53]~q\);

-- Location: LCCOMB_X36_Y14_N18
\TRIV_Schematic_inst|inst|dtm|reg|memory~23\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~23_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][53]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][53]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~23_combout\);

-- Location: FF_X36_Y14_N19
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][54]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~23_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][54]~q\);

-- Location: LCCOMB_X36_Y14_N30
\TRIV_Schematic_inst|inst|dtm|reg|memory~21\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~21_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][54]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][54]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~21_combout\);

-- Location: FF_X36_Y14_N31
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][55]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~21_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][55]~q\);

-- Location: LCCOMB_X36_Y14_N14
\TRIV_Schematic_inst|inst|dtm|reg|memory~19\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~19_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][55]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][55]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~19_combout\);

-- Location: FF_X36_Y14_N15
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][56]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~19_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][56]~q\);

-- Location: LCCOMB_X37_Y14_N6
\TRIV_Schematic_inst|inst|dtm|reg|memory~17\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~17_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][56]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][56]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~17_combout\);

-- Location: FF_X37_Y14_N7
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][57]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~17_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][57]~q\);

-- Location: LCCOMB_X37_Y14_N10
\TRIV_Schematic_inst|inst|dtm|reg|memory~15\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~15_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][57]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][57]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~15_combout\);

-- Location: FF_X37_Y14_N11
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][58]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~15_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][58]~q\);

-- Location: LCCOMB_X37_Y14_N12
\TRIV_Schematic_inst|inst|dtm|reg|memory~13\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~13_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][58]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][58]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~13_combout\);

-- Location: FF_X37_Y14_N13
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][59]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~13_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][59]~q\);

-- Location: LCCOMB_X37_Y14_N24
\TRIV_Schematic_inst|inst|dtm|reg|memory~11\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~11_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][59]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][59]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~11_combout\);

-- Location: FF_X37_Y14_N25
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][60]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~11_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][60]~q\);

-- Location: LCCOMB_X37_Y14_N0
\TRIV_Schematic_inst|inst|dtm|reg|memory~9\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~9_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][60]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][60]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~9_combout\);

-- Location: FF_X37_Y14_N1
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][61]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~9_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][61]~q\);

-- Location: LCCOMB_X37_Y14_N16
\TRIV_Schematic_inst|inst|dtm|reg|memory~7\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~7_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][61]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][61]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~7_combout\);

-- Location: FF_X37_Y14_N17
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][62]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~7_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][62]~q\);

-- Location: LCCOMB_X37_Y14_N20
\TRIV_Schematic_inst|inst|dtm|reg|memory~5\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~5_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][62]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][62]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~5_combout\);

-- Location: FF_X37_Y14_N21
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][63]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~5_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][63]~q\);

-- Location: LCCOMB_X37_Y14_N2
\TRIV_Schematic_inst|inst|dtm|reg|memory~3\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~3_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][63]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][63]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~3_combout\);

-- Location: FF_X37_Y14_N3
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][64]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~3_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][64]~q\);

-- Location: LCCOMB_X38_Y14_N24
\TRIV_Schematic_inst|inst|dtm|reg|memory~1\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~1_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][64]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][64]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~1_combout\);

-- Location: FF_X38_Y14_N25
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][65]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~1_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][65]~q\);

-- Location: LCCOMB_X38_Y14_N26
\TRIV_Schematic_inst|inst|dtm|reg|memory~88\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~88_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][65]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][65]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~88_combout\);

-- Location: FF_X38_Y14_N27
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][66]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~88_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][66]~q\);

-- Location: LCCOMB_X38_Y14_N30
\TRIV_Schematic_inst|inst|dtm|reg|memory~86\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~86_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][66]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][66]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~86_combout\);

-- Location: FF_X38_Y14_N31
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][67]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~86_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][67]~q\);

-- Location: LCCOMB_X38_Y14_N28
\TRIV_Schematic_inst|inst|dtm|reg|memory~84\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~84_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][67]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][67]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~84_combout\);

-- Location: FF_X38_Y14_N29
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][68]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~84_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][68]~q\);

-- Location: LCCOMB_X37_Y14_N26
\TRIV_Schematic_inst|inst|dtm|reg|memory~82\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~82_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][68]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][68]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~82_combout\);

-- Location: FF_X37_Y14_N27
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][69]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~82_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][69]~q\);

-- Location: LCCOMB_X37_Y14_N22
\TRIV_Schematic_inst|inst|dtm|reg|memory~80\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~80_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][69]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][69]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~80_combout\);

-- Location: FF_X37_Y14_N23
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][70]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~80_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][70]~q\);

-- Location: LCCOMB_X37_Y14_N14
\TRIV_Schematic_inst|inst|dtm|reg|memory~78\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~78_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][70]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][70]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~78_combout\);

-- Location: FF_X37_Y14_N15
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][71]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~78_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][71]~q\);

-- Location: LCCOMB_X38_Y14_N8
\TRIV_Schematic_inst|inst|dtm|reg|memory~76\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~76_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][71]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][71]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~76_combout\);

-- Location: FF_X38_Y14_N9
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][72]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~76_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][72]~q\);

-- Location: LCCOMB_X38_Y14_N12
\TRIV_Schematic_inst|inst|dtm|reg|memory~74\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~74_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][72]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][72]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~74_combout\);

-- Location: FF_X38_Y14_N13
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][73]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~74_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][73]~q\);

-- Location: LCCOMB_X38_Y14_N6
\TRIV_Schematic_inst|inst|dtm|reg|memory~72\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~72_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][73]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][73]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~72_combout\);

-- Location: FF_X38_Y14_N7
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][74]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~72_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][74]~q\);

-- Location: LCCOMB_X38_Y14_N10
\TRIV_Schematic_inst|inst|dtm|reg|memory~70\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~70_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][74]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][74]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~70_combout\);

-- Location: FF_X38_Y14_N11
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][75]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~70_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][75]~q\);

-- Location: LCCOMB_X38_Y14_N0
\TRIV_Schematic_inst|inst|dtm|reg|memory~68\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~68_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][75]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][75]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~68_combout\);

-- Location: FF_X38_Y14_N1
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][76]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~68_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][76]~q\);

-- Location: LCCOMB_X38_Y14_N4
\TRIV_Schematic_inst|inst|dtm|reg|memory~66\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~66_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][76]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][76]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~66_combout\);

-- Location: FF_X38_Y14_N5
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][77]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~66_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][77]~q\);

-- Location: LCCOMB_X38_Y14_N22
\TRIV_Schematic_inst|inst|dtm|reg|memory~64\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~64_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][77]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][77]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~64_combout\);

-- Location: FF_X38_Y14_N23
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][78]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~64_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][78]~q\);

-- Location: LCCOMB_X38_Y14_N18
\TRIV_Schematic_inst|inst|dtm|reg|memory~62\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~62_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][78]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][78]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~62_combout\);

-- Location: FF_X38_Y14_N19
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][79]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~62_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][79]~q\);

-- Location: LCCOMB_X38_Y14_N20
\TRIV_Schematic_inst|inst|dtm|reg|memory~60\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~60_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][79]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][79]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~60_combout\);

-- Location: FF_X38_Y14_N21
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][80]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~60_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][80]~q\);

-- Location: LCCOMB_X38_Y14_N2
\TRIV_Schematic_inst|inst|dtm|reg|memory~58\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~58_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][80]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][80]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~58_combout\);

-- Location: FF_X38_Y14_N3
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][81]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~58_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][81]~q\);

-- Location: LCCOMB_X38_Y14_N16
\TRIV_Schematic_inst|inst|dtm|reg|memory~56\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~56_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][81]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][81]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~56_combout\);

-- Location: FF_X38_Y14_N17
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][82]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~56_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][82]~q\);

-- Location: LCCOMB_X38_Y14_N14
\TRIV_Schematic_inst|inst|dtm|reg|memory~54\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~54_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][82]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][82]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~54_combout\);

-- Location: FF_X38_Y14_N15
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][83]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~54_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][83]~q\);

-- Location: LCCOMB_X38_Y15_N8
\TRIV_Schematic_inst|inst|dtm|reg|memory~52\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~52_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][83]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][83]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~52_combout\);

-- Location: FF_X38_Y15_N9
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][84]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~52_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][84]~q\);

-- Location: LCCOMB_X38_Y15_N12
\TRIV_Schematic_inst|inst|dtm|reg|memory~50\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~50_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][84]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][84]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~50_combout\);

-- Location: FF_X38_Y15_N13
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][85]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~50_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][85]~q\);

-- Location: LCCOMB_X37_Y15_N6
\TRIV_Schematic_inst|inst|dtm|reg|memory~48\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~48_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][85]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][85]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~48_combout\);

-- Location: FF_X37_Y15_N7
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][86]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~48_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][86]~q\);

-- Location: LCCOMB_X37_Y15_N28
\TRIV_Schematic_inst|inst|dtm|reg|memory~46\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~46_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][86]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][86]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~46_combout\);

-- Location: FF_X37_Y15_N29
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][87]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~46_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][87]~q\);

-- Location: LCCOMB_X37_Y14_N18
\TRIV_Schematic_inst|inst|dtm|reg|memory~44\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~44_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][87]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][87]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~44_combout\);

-- Location: FF_X37_Y14_N19
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][88]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~44_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][88]~q\);

-- Location: LCCOMB_X37_Y14_N30
\TRIV_Schematic_inst|inst|dtm|reg|memory~42\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~42_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][88]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110000001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][88]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~42_combout\);

-- Location: FF_X37_Y14_N31
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][89]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~42_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][89]~q\);

-- Location: LCCOMB_X37_Y14_N8
\TRIV_Schematic_inst|inst|dtm|reg|memory~40\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~40_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][89]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][89]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~40_combout\);

-- Location: FF_X37_Y14_N9
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][90]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~40_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][90]~q\);

-- Location: LCCOMB_X37_Y14_N4
\TRIV_Schematic_inst|inst|dtm|reg|memory~38\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~38_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][90]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][90]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~38_combout\);

-- Location: FF_X37_Y14_N5
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][91]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~38_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][91]~q\);

-- Location: LCCOMB_X37_Y14_N28
\TRIV_Schematic_inst|inst|dtm|reg|memory~36\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~36_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][91]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][91]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~36_combout\);

-- Location: FF_X37_Y14_N29
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][92]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~36_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][92]~q\);

-- Location: LCCOMB_X37_Y17_N12
\TRIV_Schematic_inst|inst|dtm|reg|memory~34\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~34_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][92]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][92]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~34_combout\);

-- Location: FF_X37_Y17_N13
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][93]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~34_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][93]~q\);

-- Location: LCCOMB_X37_Y17_N30
\TRIV_Schematic_inst|inst|dtm|reg|memory~32\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~32_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][93]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][93]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~32_combout\);

-- Location: FF_X37_Y17_N31
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][94]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~32_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][94]~q\);

-- Location: LCCOMB_X37_Y17_N0
\TRIV_Schematic_inst|inst|dtm|reg|memory~30\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~30_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][94]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][94]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~30_combout\);

-- Location: FF_X37_Y17_N1
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][95]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~30_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][95]~q\);

-- Location: LCCOMB_X37_Y17_N2
\TRIV_Schematic_inst|inst|dtm|reg|memory~28\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~28_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][95]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][95]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~28_combout\);

-- Location: FF_X37_Y17_N3
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][96]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~28_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][96]~q\);

-- Location: LCCOMB_X37_Y17_N16
\TRIV_Schematic_inst|inst|dtm|reg|memory~26\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~26_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][96]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][96]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~26_combout\);

-- Location: FF_X37_Y17_N17
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][97]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~26_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][97]~q\);

-- Location: LCCOMB_X37_Y17_N14
\TRIV_Schematic_inst|inst|dtm|reg|memory~24\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~24_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][97]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][97]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~24_combout\);

-- Location: FF_X37_Y17_N15
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][98]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~24_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][98]~q\);

-- Location: LCCOMB_X37_Y17_N20
\TRIV_Schematic_inst|inst|dtm|reg|memory~22\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~22_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][98]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][98]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~22_combout\);

-- Location: FF_X37_Y17_N21
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][99]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~22_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][99]~q\);

-- Location: LCCOMB_X37_Y17_N24
\TRIV_Schematic_inst|inst|dtm|reg|memory~20\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~20_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][99]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][99]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~20_combout\);

-- Location: FF_X37_Y17_N25
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][100]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~20_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][100]~q\);

-- Location: LCCOMB_X37_Y17_N18
\TRIV_Schematic_inst|inst|dtm|reg|memory~18\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~18_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][100]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][100]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~18_combout\);

-- Location: FF_X37_Y17_N19
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][101]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~18_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][101]~q\);

-- Location: LCCOMB_X37_Y17_N26
\TRIV_Schematic_inst|inst|dtm|reg|memory~16\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~16_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][101]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][101]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~16_combout\);

-- Location: FF_X37_Y17_N27
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][102]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~16_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][102]~q\);

-- Location: LCCOMB_X37_Y17_N4
\TRIV_Schematic_inst|inst|dtm|reg|memory~14\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~14_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][102]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][102]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~14_combout\);

-- Location: FF_X37_Y17_N5
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][103]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~14_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][103]~q\);

-- Location: LCCOMB_X37_Y17_N22
\TRIV_Schematic_inst|inst|dtm|reg|memory~12\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~12_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][103]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][103]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~12_combout\);

-- Location: FF_X37_Y17_N23
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][104]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~12_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][104]~q\);

-- Location: LCCOMB_X37_Y17_N6
\TRIV_Schematic_inst|inst|dtm|reg|memory~10\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~10_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][104]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][104]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~10_combout\);

-- Location: FF_X37_Y17_N7
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][105]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~10_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][105]~q\);

-- Location: LCCOMB_X37_Y15_N14
\TRIV_Schematic_inst|inst|dtm|reg|memory~8\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~8_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\ & \TRIV_Schematic_inst|inst|dtm|reg|memory[0][105]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][105]~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~8_combout\);

-- Location: FF_X37_Y15_N15
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][106]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~8_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][106]~q\);

-- Location: LCCOMB_X38_Y15_N4
\TRIV_Schematic_inst|inst|dtm|reg|memory~6\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~6_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][106]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][106]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~6_combout\);

-- Location: FF_X38_Y15_N5
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][107]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~6_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][107]~q\);

-- Location: LCCOMB_X38_Y15_N14
\TRIV_Schematic_inst|inst|dtm|reg|memory~4\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~4_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][107]~q\) # (\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][107]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~4_combout\);

-- Location: FF_X38_Y15_N15
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][108]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~4_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][108]~q\);

-- Location: LCCOMB_X38_Y15_N2
\TRIV_Schematic_inst|inst|dtm|reg|memory~2\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~2_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][108]~q\) # (\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][108]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~2_combout\);

-- Location: FF_X38_Y15_N3
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][109]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~2_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][109]~q\);

-- Location: LCCOMB_X38_Y15_N6
\TRIV_Schematic_inst|inst|dtm|reg|memory~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|reg|memory~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][109]~q\) # (\TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][109]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|load_init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|reg|memory~0_combout\);

-- Location: FF_X38_Y15_N7
\TRIV_Schematic_inst|inst|dtm|reg|memory[0][110]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|reg|memory~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	ena => \TRIV_Schematic_inst|inst|dtm|uc_triv|load~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][110]~q\);

-- Location: LCCOMB_X38_Y15_N16
\TRIV_Schematic_inst|inst|dtm|talu|out_z\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|talu|out_z~combout\ = \TRIV_Schematic_inst|inst|dtm|reg|memory[0][110]~q\ $ (\TRIV_Schematic_inst|inst|dtm|reg|memory[0][65]~q\ $ (\TRIV_Schematic_inst|inst|dtm|talu|t1_1~combout\ $ 
-- (\TRIV_Schematic_inst|inst|dtm|talu|t2_1~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110100110010110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][110]~q\,
	datab => \TRIV_Schematic_inst|inst|dtm|reg|memory[0][65]~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|talu|t1_1~combout\,
	datad => \TRIV_Schematic_inst|inst|dtm|talu|t2_1~combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|talu|out_z~combout\);

-- Location: LCCOMB_X39_Y16_N26
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~1\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~1_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.warmup~q\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~62_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[31]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.warmup~q\ & (((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[31]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.warmup~q\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~62_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[31]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~1_combout\);

-- Location: FF_X39_Y16_N27
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~1_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[31]~q\);

-- Location: LCCOMB_X38_Y17_N0
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~0_combout\ = \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[0]~q\ $ (VCC)
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~1\ = CARRY(\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[0]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[0]~q\,
	datad => VCC,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~0_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~1\);

-- Location: LCCOMB_X39_Y17_N12
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~7_combout\ & \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.warmup~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~7_combout\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.warmup~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\);

-- Location: LCCOMB_X39_Y17_N22
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector31~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector31~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~0_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[0]~q\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~0_combout\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[0]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~0_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[0]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector31~0_combout\);

-- Location: FF_X39_Y17_N23
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector31~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[0]~q\);

-- Location: LCCOMB_X38_Y17_N2
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~2\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~2_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[1]~q\ & (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~1\)) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[1]~q\ & 
-- ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~1\) # (GND)))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~3\ = CARRY((!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~1\) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[1]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[1]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~1\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~2_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~3\);

-- Location: LCCOMB_X39_Y17_N2
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector30~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector30~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[1]~q\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~2_combout\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~2_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~2_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[1]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector30~0_combout\);

-- Location: FF_X39_Y17_N3
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector30~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[1]~q\);

-- Location: LCCOMB_X38_Y17_N4
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~4\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~4_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[2]~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~3\ $ (GND))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[2]~q\ & 
-- (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~3\ & VCC))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~5\ = CARRY((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[2]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[2]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~3\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~4_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~5\);

-- Location: LCCOMB_X39_Y17_N18
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector29~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector29~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[2]~q\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~4_combout\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~4_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~4_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[2]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector29~0_combout\);

-- Location: FF_X39_Y17_N19
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector29~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[2]~q\);

-- Location: LCCOMB_X38_Y17_N6
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~6\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~6_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[3]~q\ & (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~5\)) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[3]~q\ & 
-- ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~5\) # (GND)))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~7\ = CARRY((!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~5\) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[3]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[3]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~5\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~6_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~7\);

-- Location: LCCOMB_X39_Y17_N24
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector28~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector28~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[3]~q\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~6_combout\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~6_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~6_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[3]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector28~0_combout\);

-- Location: FF_X39_Y17_N25
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector28~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[3]~q\);

-- Location: LCCOMB_X38_Y17_N8
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~8\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~8_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[4]~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~7\ $ (GND))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[4]~q\ & 
-- (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~7\ & VCC))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~9\ = CARRY((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[4]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[4]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~7\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~8_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~9\);

-- Location: LCCOMB_X39_Y17_N6
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector27~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector27~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[4]~q\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~8_combout\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~8_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~8_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[4]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector27~0_combout\);

-- Location: FF_X39_Y17_N7
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector27~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[4]~q\);

-- Location: LCCOMB_X38_Y17_N10
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~10\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~10_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[5]~q\ & (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~9\)) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[5]~q\ & 
-- ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~9\) # (GND)))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~11\ = CARRY((!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~9\) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[5]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[5]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~9\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~10_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~11\);

-- Location: LCCOMB_X39_Y17_N28
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector26~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector26~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[5]~q\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~10_combout\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~10_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~10_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[5]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector26~0_combout\);

-- Location: FF_X39_Y17_N29
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector26~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[5]~q\);

-- Location: LCCOMB_X38_Y17_N12
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~12\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~12_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[6]~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~11\ $ (GND))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[6]~q\ & 
-- (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~11\ & VCC))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~13\ = CARRY((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[6]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[6]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~11\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~12_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~13\);

-- Location: LCCOMB_X39_Y17_N8
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector25~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector25~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[6]~q\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~12_combout\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~12_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~12_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[6]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector25~0_combout\);

-- Location: FF_X39_Y17_N9
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector25~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[6]~q\);

-- Location: LCCOMB_X38_Y17_N14
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~14\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~14_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[7]~q\ & (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~13\)) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[7]~q\ & 
-- ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~13\) # (GND)))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~15\ = CARRY((!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~13\) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[7]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[7]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~13\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~14_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~15\);

-- Location: LCCOMB_X39_Y17_N4
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector24~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector24~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[7]~q\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~14_combout\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~14_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~14_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[7]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector24~0_combout\);

-- Location: FF_X39_Y17_N5
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector24~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[7]~q\);

-- Location: LCCOMB_X38_Y17_N16
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~16\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~16_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[8]~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~15\ $ (GND))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[8]~q\ & 
-- (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~15\ & VCC))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~17\ = CARRY((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[8]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~15\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[8]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~15\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~16_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~17\);

-- Location: LCCOMB_X39_Y17_N16
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector23~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector23~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~16_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[8]~q\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~16_combout\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[8]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~16_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[8]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector23~0_combout\);

-- Location: FF_X39_Y17_N17
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector23~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[8]~q\);

-- Location: LCCOMB_X38_Y17_N18
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~18\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~18_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[9]~q\ & (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~17\)) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[9]~q\ & 
-- ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~17\) # (GND)))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~19\ = CARRY((!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~17\) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[9]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[9]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~17\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~18_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~19\);

-- Location: LCCOMB_X39_Y17_N0
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector22~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector22~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[9]~q\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~18_combout\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~18_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~18_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[9]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector22~0_combout\);

-- Location: FF_X39_Y17_N1
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector22~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[9]~q\);

-- Location: LCCOMB_X38_Y17_N20
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~20\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~20_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[10]~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~19\ $ (GND))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[10]~q\ & 
-- (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~19\ & VCC))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~21\ = CARRY((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[10]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[10]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~19\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~20_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~21\);

-- Location: LCCOMB_X39_Y17_N20
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector21~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector21~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[10]~q\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~20_combout\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~20_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110110010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~20_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[10]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~0_combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector21~0_combout\);

-- Location: FF_X39_Y17_N21
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector21~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[10]~q\);

-- Location: LCCOMB_X38_Y17_N22
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~22\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~22_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[11]~q\ & (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~21\)) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[11]~q\ & 
-- ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~21\) # (GND)))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~23\ = CARRY((!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~21\) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[11]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[11]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~21\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~22_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~23\);

-- Location: LCCOMB_X37_Y16_N0
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector20~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector20~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~22_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[11]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & (((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[11]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~22_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[11]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector20~0_combout\);

-- Location: FF_X37_Y16_N1
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector20~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[11]~q\);

-- Location: LCCOMB_X38_Y17_N24
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~24\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~24_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[12]~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~23\ $ (GND))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[12]~q\ & 
-- (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~23\ & VCC))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~25\ = CARRY((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[12]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[12]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~23\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~24_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~25\);

-- Location: LCCOMB_X37_Y16_N26
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector19~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector19~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~24_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[12]~q\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & 
-- (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[12]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[12]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~24_combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector19~0_combout\);

-- Location: FF_X37_Y16_N27
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector19~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[12]~q\);

-- Location: LCCOMB_X38_Y17_N26
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~26\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~26_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[13]~q\ & (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~25\)) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[13]~q\ & 
-- ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~25\) # (GND)))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~27\ = CARRY((!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~25\) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[13]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[13]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~25\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~26_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~27\);

-- Location: LCCOMB_X37_Y16_N8
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector18~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector18~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~26_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[13]~q\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & 
-- (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[13]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[13]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~26_combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector18~0_combout\);

-- Location: FF_X37_Y16_N9
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector18~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[13]~q\);

-- Location: LCCOMB_X38_Y17_N28
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~28\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~28_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[14]~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~27\ $ (GND))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[14]~q\ & 
-- (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~27\ & VCC))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~29\ = CARRY((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[14]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~27\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[14]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~27\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~28_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~29\);

-- Location: LCCOMB_X37_Y16_N14
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector17~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector17~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~28_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[14]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & (((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[14]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~28_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[14]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector17~0_combout\);

-- Location: FF_X37_Y16_N15
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector17~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[14]~q\);

-- Location: LCCOMB_X38_Y17_N30
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~30\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~30_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[15]~q\ & (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~29\)) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[15]~q\ & 
-- ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~29\) # (GND)))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~31\ = CARRY((!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~29\) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[15]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[15]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~29\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~30_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~31\);

-- Location: LCCOMB_X37_Y16_N4
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector16~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector16~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~30_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[15]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & (((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[15]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~30_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[15]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector16~0_combout\);

-- Location: FF_X37_Y16_N5
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector16~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[15]~q\);

-- Location: LCCOMB_X38_Y16_N0
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~32\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~32_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[16]~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~31\ $ (GND))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[16]~q\ & 
-- (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~31\ & VCC))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~33\ = CARRY((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[16]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[16]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~31\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~32_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~33\);

-- Location: LCCOMB_X37_Y16_N16
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector15~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector15~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~32_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[16]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & (((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[16]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~32_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[16]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector15~0_combout\);

-- Location: FF_X37_Y16_N17
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector15~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[16]~q\);

-- Location: LCCOMB_X38_Y16_N2
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~34\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~34_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[17]~q\ & (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~33\)) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[17]~q\ & 
-- ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~33\) # (GND)))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~35\ = CARRY((!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~33\) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[17]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[17]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~33\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~34_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~35\);

-- Location: LCCOMB_X37_Y16_N6
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector14~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector14~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~34_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[17]~q\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & 
-- (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[17]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[17]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~34_combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector14~0_combout\);

-- Location: FF_X37_Y16_N7
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector14~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[17]~q\);

-- Location: LCCOMB_X38_Y16_N4
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~36\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~36_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[18]~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~35\ $ (GND))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[18]~q\ & 
-- (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~35\ & VCC))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~37\ = CARRY((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[18]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[18]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~35\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~36_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~37\);

-- Location: LCCOMB_X37_Y16_N22
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector13~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector13~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~36_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[18]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & (((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[18]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~36_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[18]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector13~0_combout\);

-- Location: FF_X37_Y16_N23
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector13~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[18]~q\);

-- Location: LCCOMB_X38_Y16_N6
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~38\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~38_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[19]~q\ & (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~37\)) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[19]~q\ & 
-- ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~37\) # (GND)))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~39\ = CARRY((!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~37\) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[19]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[19]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~37\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~38_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~39\);

-- Location: LCCOMB_X37_Y16_N20
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector12~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector12~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~38_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[19]~q\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\ & 
-- (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[19]~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110101011000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[19]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~38_combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector12~0_combout\);

-- Location: FF_X37_Y16_N21
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector12~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[19]~q\);

-- Location: LCCOMB_X38_Y16_N8
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~40\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~40_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[20]~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~39\ $ (GND))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[20]~q\ & 
-- (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~39\ & VCC))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~41\ = CARRY((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[20]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[20]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~39\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~40_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~41\);

-- Location: LCCOMB_X37_Y16_N30
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector11~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector11~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~40_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[20]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & (((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[20]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~40_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[20]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector11~0_combout\);

-- Location: FF_X37_Y16_N31
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector11~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[20]~q\);

-- Location: LCCOMB_X38_Y16_N10
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~42\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~42_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[21]~q\ & (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~41\)) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[21]~q\ & 
-- ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~41\) # (GND)))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~43\ = CARRY((!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~41\) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[21]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[21]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~41\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~42_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~43\);

-- Location: LCCOMB_X37_Y16_N18
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector10~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector10~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~42_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[21]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & (((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[21]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~42_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[21]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector10~0_combout\);

-- Location: FF_X37_Y16_N19
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector10~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[21]~q\);

-- Location: LCCOMB_X38_Y16_N12
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~44\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~44_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[22]~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~43\ $ (GND))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[22]~q\ & 
-- (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~43\ & VCC))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~45\ = CARRY((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[22]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~43\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[22]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~43\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~44_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~45\);

-- Location: LCCOMB_X37_Y16_N2
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector9~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector9~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~44_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[22]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & (((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[22]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~44_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[22]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector9~0_combout\);

-- Location: FF_X37_Y16_N3
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector9~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[22]~q\);

-- Location: LCCOMB_X38_Y16_N14
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~46\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~46_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[23]~q\ & (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~45\)) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[23]~q\ & 
-- ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~45\) # (GND)))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~47\ = CARRY((!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~45\) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[23]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[23]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~45\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~46_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~47\);

-- Location: LCCOMB_X39_Y16_N2
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector8~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector8~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~46_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[23]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~46_combout\ & (((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[23]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~46_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[23]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector8~0_combout\);

-- Location: FF_X39_Y16_N3
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector8~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[23]~q\);

-- Location: LCCOMB_X38_Y16_N16
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~48\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~48_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[24]~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~47\ $ (GND))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[24]~q\ & 
-- (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~47\ & VCC))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~49\ = CARRY((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[24]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~47\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[24]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~47\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~48_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~49\);

-- Location: LCCOMB_X39_Y16_N16
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector7~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector7~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~48_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[24]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~48_combout\ & (((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[24]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~48_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[24]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector7~0_combout\);

-- Location: FF_X39_Y16_N17
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector7~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[24]~q\);

-- Location: LCCOMB_X38_Y16_N18
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~50\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~50_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[25]~q\ & (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~49\)) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[25]~q\ & 
-- ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~49\) # (GND)))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~51\ = CARRY((!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~49\) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[25]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[25]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~49\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~50_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~51\);

-- Location: LCCOMB_X39_Y16_N14
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector6~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector6~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~50_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[25]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & (((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[25]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~50_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[25]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector6~0_combout\);

-- Location: FF_X39_Y16_N15
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector6~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[25]~q\);

-- Location: LCCOMB_X38_Y16_N20
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~52\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~52_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[26]~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~51\ $ (GND))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[26]~q\ & 
-- (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~51\ & VCC))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~53\ = CARRY((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[26]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~51\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[26]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~51\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~52_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~53\);

-- Location: LCCOMB_X39_Y16_N28
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector5~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector5~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~52_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[26]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & (((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[26]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~52_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[26]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector5~0_combout\);

-- Location: FF_X39_Y16_N29
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector5~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[26]~q\);

-- Location: LCCOMB_X38_Y16_N22
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~54\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~54_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[27]~q\ & (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~53\)) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[27]~q\ & 
-- ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~53\) # (GND)))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~55\ = CARRY((!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~53\) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[27]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[27]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~53\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~54_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~55\);

-- Location: LCCOMB_X38_Y16_N24
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~56\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~56_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[28]~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~55\ $ (GND))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[28]~q\ & 
-- (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~55\ & VCC))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~57\ = CARRY((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[28]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~55\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[28]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~55\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~56_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~57\);

-- Location: LCCOMB_X39_Y16_N0
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector3~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector3~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~56_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[28]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & (((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[28]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~56_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[28]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector3~0_combout\);

-- Location: FF_X39_Y16_N1
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector3~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[28]~q\);

-- Location: LCCOMB_X38_Y16_N26
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~58\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~58_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[29]~q\ & (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~57\)) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[29]~q\ & 
-- ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~57\) # (GND)))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~59\ = CARRY((!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~57\) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[29]~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[29]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~57\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~58_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~59\);

-- Location: LCCOMB_X39_Y16_N30
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector2~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector2~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~58_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[29]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~58_combout\ & (((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[29]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~58_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[29]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector2~0_combout\);

-- Location: FF_X39_Y16_N31
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector2~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[29]~q\);

-- Location: LCCOMB_X38_Y16_N28
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~60\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~60_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[30]~q\ & (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~59\ $ (GND))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[30]~q\ & 
-- (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~59\ & VCC))
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~61\ = CARRY((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[30]~q\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~59\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[30]~q\,
	datad => VCC,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~59\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~60_combout\,
	cout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~61\);

-- Location: LCCOMB_X39_Y16_N20
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector1~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector1~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~60_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[30]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & (((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[30]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~60_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[30]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector1~0_combout\);

-- Location: FF_X39_Y16_N21
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector1~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[30]~q\);

-- Location: LCCOMB_X38_Y16_N30
\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~62\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~62_combout\ = \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~61\ $ (\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[31]~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[31]~q\,
	cin => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~61\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~62_combout\);

-- Location: LCCOMB_X39_Y16_N22
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.warmup~q\ & \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~62_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.warmup~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~62_combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\);

-- Location: LCCOMB_X39_Y16_N18
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector4~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector4~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~54_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[27]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\)))) # (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\ & (((\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[27]~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector0~0_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~54_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[27]~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector4~0_combout\);

-- Location: FF_X39_Y16_N19
\TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector4~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|new_fsm:counter[27]~q\);

-- Location: LCCOMB_X39_Y16_N12
\TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~6\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~6_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~54_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~56_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~58_combout\) # 
-- (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~60_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~54_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~56_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~58_combout\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~60_combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~6_combout\);

-- Location: LCCOMB_X37_Y16_N10
\TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~3\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~3_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~44_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~42_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~40_combout\) # 
-- (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~38_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~44_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~42_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~40_combout\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~38_combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~3_combout\);

-- Location: LCCOMB_X37_Y16_N24
\TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~2\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~2_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~30_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~32_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~36_combout\) # 
-- (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~34_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~30_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~32_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~36_combout\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~34_combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~2_combout\);

-- Location: LCCOMB_X39_Y17_N10
\TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~20_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~14_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~16_combout\) # 
-- (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~18_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~14_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~20_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~16_combout\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~18_combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~0_combout\);

-- Location: LCCOMB_X37_Y16_N12
\TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~1\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~1_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~26_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~22_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~28_combout\) # 
-- (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~24_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~26_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~22_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~28_combout\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~24_combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~1_combout\);

-- Location: LCCOMB_X37_Y16_N28
\TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~4\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~4_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~3_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~2_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~0_combout\) # 
-- (\TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~3_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~2_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~0_combout\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~1_combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~4_combout\);

-- Location: LCCOMB_X39_Y16_N24
\TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~5\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~5_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~46_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~52_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~48_combout\) # 
-- (\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~50_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~46_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~52_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~48_combout\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~50_combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~5_combout\);

-- Location: LCCOMB_X39_Y16_N8
\TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~7\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~7_combout\ = (!\TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~62_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~6_combout\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~4_combout\) # 
-- (\TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~5_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~6_combout\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|Add0~62_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~4_combout\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~5_combout\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~7_combout\);

-- Location: LCCOMB_X39_Y17_N30
\TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~1\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~1_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~7_combout\ & (\bMKR_D[1]~input_o\ & ((!\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.init~q\)))) # 
-- (!\TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~7_combout\ & ((\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.warmup~q\) # ((\bMKR_D[1]~input_o\ & !\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.init~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000011011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~7_combout\,
	datab => \bMKR_D[1]~input_o\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.warmup~q\,
	datad => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.init~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~1_combout\);

-- Location: FF_X39_Y17_N31
\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.warmup\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|Selector33~1_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.warmup~q\);

-- Location: LCCOMB_X39_Y16_N10
\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~0\ : cyclone10lp_lcell_comb
-- Equation(s):
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~0_combout\ = (\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\) # ((\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.warmup~q\ & 
-- \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~7_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111100011111000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.warmup~q\,
	datab => \TRIV_Schematic_inst|inst|dtm|uc_triv|LessThan0~7_combout\,
	datac => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\,
	combout => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~0_combout\);

-- Location: FF_X39_Y16_N11
\TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~0_combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|pr_state.generatekey~q\);

-- Location: FF_X38_Y15_N17
\TRIV_Schematic_inst|inst|dtm|uc_triv|bit_ciphered\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \TRIV_Schematic_inst|inst|s_clk_1hz~clkctrl_outclk\,
	d => \TRIV_Schematic_inst|inst|dtm|talu|out_z~combout\,
	clrn => \ALT_INV_bMKR_D[7]~input_o\,
	sclr => \TRIV_Schematic_inst|inst|dtm|uc_triv|ALT_INV_pr_state.generatekey~q\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \TRIV_Schematic_inst|inst|dtm|uc_triv|bit_ciphered~q\);

-- Location: IOIBUF_X0_Y14_N1
\iCLK~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_iCLK,
	o => \iCLK~input_o\);

-- Location: PLL_1
\PLL_inst|altpll_component|auto_generated|pll1\ : cyclone10lp_pll
-- pragma translate_off
GENERIC MAP (
	auto_settings => "false",
	bandwidth_type => "medium",
	c0_high => 3,
	c0_initial => 4,
	c0_low => 3,
	c0_mode => "even",
	c0_ph => 0,
	c1_high => 0,
	c1_initial => 0,
	c1_low => 0,
	c1_mode => "bypass",
	c1_ph => 0,
	c1_use_casc_in => "off",
	c2_high => 0,
	c2_initial => 0,
	c2_low => 0,
	c2_mode => "bypass",
	c2_ph => 0,
	c2_use_casc_in => "off",
	c3_high => 0,
	c3_initial => 0,
	c3_low => 0,
	c3_mode => "bypass",
	c3_ph => 0,
	c3_use_casc_in => "off",
	c4_high => 0,
	c4_initial => 0,
	c4_low => 0,
	c4_mode => "bypass",
	c4_ph => 0,
	c4_use_casc_in => "off",
	charge_pump_current_bits => 1,
	clk0_counter => "unused",
	clk0_divide_by => 0,
	clk0_duty_cycle => 50,
	clk0_multiply_by => 0,
	clk0_phase_shift => "0",
	clk1_counter => "unused",
	clk1_divide_by => 0,
	clk1_duty_cycle => 50,
	clk1_multiply_by => 0,
	clk1_phase_shift => "0",
	clk2_counter => "unused",
	clk2_divide_by => 0,
	clk2_duty_cycle => 50,
	clk2_multiply_by => 0,
	clk2_phase_shift => "0",
	clk3_counter => "c0",
	clk3_divide_by => 12,
	clk3_duty_cycle => 50,
	clk3_multiply_by => 25,
	clk3_phase_shift => "5000",
	clk4_counter => "unused",
	clk4_divide_by => 0,
	clk4_duty_cycle => 50,
	clk4_multiply_by => 0,
	clk4_phase_shift => "0",
	compensate_clock => "clock3",
	inclk0_input_frequency => 20833,
	inclk1_input_frequency => 0,
	loop_filter_c_bits => 0,
	loop_filter_r_bits => 24,
	m => 25,
	m_initial => 1,
	m_ph => 0,
	n => 2,
	operation_mode => "normal",
	pfd_max => 200000,
	pfd_min => 3076,
	self_reset_on_loss_lock => "off",
	simulation_type => "timing",
	switch_over_type => "auto",
	vco_center => 1538,
	vco_divide_by => 0,
	vco_frequency_control => "auto",
	vco_max => 3333,
	vco_min => 1538,
	vco_multiply_by => 0,
	vco_phase_shift_step => 208,
	vco_post_scale => 2)
-- pragma translate_on
PORT MAP (
	areset => GND,
	fbin => \PLL_inst|altpll_component|auto_generated|wire_pll1_fbout\,
	inclk => \PLL_inst|altpll_component|auto_generated|pll1_INCLK_bus\,
	fbout => \PLL_inst|altpll_component|auto_generated|wire_pll1_fbout\,
	clk => \PLL_inst|altpll_component|auto_generated|pll1_CLK_bus\);

-- Location: CLKCTRL_G3
\PLL_inst|altpll_component|auto_generated|wire_pll1_clk[3]~clkctrl\ : cyclone10lp_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \PLL_inst|altpll_component|auto_generated|wire_pll1_clk[3]~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \PLL_inst|altpll_component|auto_generated|wire_pll1_clk[3]~clkctrl_outclk\);

-- Location: IOIBUF_X0_Y14_N8
\iRESETn~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_iRESETn,
	o => \iRESETn~input_o\);

-- Location: IOIBUF_X41_Y9_N15
\iSAM_INT~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_iSAM_INT,
	o => \iSAM_INT~input_o\);

-- Location: IOIBUF_X21_Y0_N15
\iPEX_PIN11~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_iPEX_PIN11,
	o => \iPEX_PIN11~input_o\);

-- Location: IOIBUF_X21_Y0_N22
\iPEX_PIN13~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_iPEX_PIN13,
	o => \iPEX_PIN13~input_o\);

-- Location: IOIBUF_X21_Y0_N1
\iPEX_PIN23~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_iPEX_PIN23,
	o => \iPEX_PIN23~input_o\);

-- Location: IOIBUF_X21_Y0_N8
\iPEX_PIN25~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_iPEX_PIN25,
	o => \iPEX_PIN25~input_o\);

-- Location: IOIBUF_X19_Y29_N1
\iPEX_PIN31~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_iPEX_PIN31,
	o => \iPEX_PIN31~input_o\);

-- Location: IOIBUF_X19_Y29_N8
\iPEX_PIN33~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_iPEX_PIN33,
	o => \iPEX_PIN33~input_o\);

-- Location: IOIBUF_X41_Y14_N1
\iWM_PIO32~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_iWM_PIO32,
	o => \iWM_PIO32~input_o\);

-- Location: IOIBUF_X41_Y15_N1
\iWM_TX~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_iWM_TX,
	o => \iWM_TX~input_o\);

-- Location: IOIBUF_X41_Y15_N22
\iHDMI_HPD~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_iHDMI_HPD,
	o => \iHDMI_HPD~input_o\);

-- Location: IOIBUF_X0_Y10_N22
\iMIPI_D[0]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_iMIPI_D(0),
	ibar => \ww_iMIPI_D[0](n)\,
	o => \iMIPI_D[0]~input_o\);

-- Location: IOIBUF_X0_Y13_N15
\iMIPI_D[1]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_iMIPI_D(1),
	ibar => \ww_iMIPI_D[1](n)\,
	o => \iMIPI_D[1]~input_o\);

-- Location: IOIBUF_X0_Y14_N15
\iMIPI_CLK~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_iMIPI_CLK,
	ibar => \ww_iMIPI_CLK(n)\,
	o => \iMIPI_CLK~input_o\);

-- Location: IOIBUF_X3_Y29_N15
\bSDRAM_DQ[0]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bSDRAM_DQ(0),
	o => \bSDRAM_DQ[0]~input_o\);

-- Location: IOIBUF_X3_Y29_N8
\bSDRAM_DQ[1]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bSDRAM_DQ(1),
	o => \bSDRAM_DQ[1]~input_o\);

-- Location: IOIBUF_X1_Y29_N1
\bSDRAM_DQ[2]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bSDRAM_DQ(2),
	o => \bSDRAM_DQ[2]~input_o\);

-- Location: IOIBUF_X3_Y29_N29
\bSDRAM_DQ[3]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bSDRAM_DQ(3),
	o => \bSDRAM_DQ[3]~input_o\);

-- Location: IOIBUF_X3_Y29_N1
\bSDRAM_DQ[4]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bSDRAM_DQ(4),
	o => \bSDRAM_DQ[4]~input_o\);

-- Location: IOIBUF_X5_Y29_N1
\bSDRAM_DQ[5]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bSDRAM_DQ(5),
	o => \bSDRAM_DQ[5]~input_o\);

-- Location: IOIBUF_X5_Y29_N15
\bSDRAM_DQ[6]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bSDRAM_DQ(6),
	o => \bSDRAM_DQ[6]~input_o\);

-- Location: IOIBUF_X9_Y29_N1
\bSDRAM_DQ[7]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bSDRAM_DQ(7),
	o => \bSDRAM_DQ[7]~input_o\);

-- Location: IOIBUF_X14_Y29_N29
\bSDRAM_DQ[8]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bSDRAM_DQ(8),
	o => \bSDRAM_DQ[8]~input_o\);

-- Location: IOIBUF_X14_Y29_N1
\bSDRAM_DQ[9]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bSDRAM_DQ(9),
	o => \bSDRAM_DQ[9]~input_o\);

-- Location: IOIBUF_X7_Y29_N8
\bSDRAM_DQ[10]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bSDRAM_DQ(10),
	o => \bSDRAM_DQ[10]~input_o\);

-- Location: IOIBUF_X14_Y29_N22
\bSDRAM_DQ[11]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bSDRAM_DQ(11),
	o => \bSDRAM_DQ[11]~input_o\);

-- Location: IOIBUF_X7_Y29_N29
\bSDRAM_DQ[12]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bSDRAM_DQ(12),
	o => \bSDRAM_DQ[12]~input_o\);

-- Location: IOIBUF_X14_Y29_N8
\bSDRAM_DQ[13]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bSDRAM_DQ(13),
	o => \bSDRAM_DQ[13]~input_o\);

-- Location: IOIBUF_X5_Y29_N22
\bSDRAM_DQ[14]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bSDRAM_DQ(14),
	o => \bSDRAM_DQ[14]~input_o\);

-- Location: IOIBUF_X9_Y29_N8
\bSDRAM_DQ[15]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bSDRAM_DQ(15),
	o => \bSDRAM_DQ[15]~input_o\);

-- Location: IOIBUF_X0_Y26_N15
\bMKR_AREF~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMKR_AREF,
	o => \bMKR_AREF~input_o\);

-- Location: IOIBUF_X0_Y25_N1
\bMKR_A[0]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMKR_A(0),
	o => \bMKR_A[0]~input_o\);

-- Location: IOIBUF_X1_Y29_N22
\bMKR_A[1]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMKR_A(1),
	o => \bMKR_A[1]~input_o\);

-- Location: IOIBUF_X11_Y29_N29
\bMKR_A[2]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMKR_A(2),
	o => \bMKR_A[2]~input_o\);

-- Location: IOIBUF_X0_Y24_N15
\bMKR_A[3]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMKR_A(3),
	o => \bMKR_A[3]~input_o\);

-- Location: IOIBUF_X1_Y29_N29
\bMKR_A[4]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMKR_A(4),
	o => \bMKR_A[4]~input_o\);

-- Location: IOIBUF_X0_Y25_N15
\bMKR_A[5]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMKR_A(5),
	o => \bMKR_A[5]~input_o\);

-- Location: IOIBUF_X0_Y21_N1
\bMKR_A[6]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMKR_A(6),
	o => \bMKR_A[6]~input_o\);

-- Location: IOIBUF_X0_Y21_N22
\bMKR_D[0]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMKR_D(0),
	o => \bMKR_D[0]~input_o\);

-- Location: IOIBUF_X3_Y0_N29
\bMKR_D[2]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMKR_D(2),
	o => \bMKR_D[2]~input_o\);

-- Location: IOIBUF_X3_Y0_N15
\bMKR_D[3]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMKR_D(3),
	o => \bMKR_D[3]~input_o\);

-- Location: IOIBUF_X3_Y0_N8
\bMKR_D[4]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMKR_D(4),
	o => \bMKR_D[4]~input_o\);

-- Location: IOIBUF_X5_Y0_N8
\bMKR_D[5]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMKR_D(5),
	o => \bMKR_D[5]~input_o\);

-- Location: IOIBUF_X41_Y19_N15
\bMKR_D[8]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMKR_D(8),
	o => \bMKR_D[8]~input_o\);

-- Location: IOIBUF_X41_Y19_N8
\bMKR_D[9]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMKR_D(9),
	o => \bMKR_D[9]~input_o\);

-- Location: IOIBUF_X41_Y27_N22
\bMKR_D[10]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMKR_D(10),
	o => \bMKR_D[10]~input_o\);

-- Location: IOIBUF_X41_Y27_N15
\bMKR_D[11]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMKR_D(11),
	o => \bMKR_D[11]~input_o\);

-- Location: IOIBUF_X41_Y19_N1
\bMKR_D[12]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMKR_D(12),
	o => \bMKR_D[12]~input_o\);

-- Location: IOIBUF_X37_Y29_N15
\bMKR_D[13]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMKR_D(13),
	o => \bMKR_D[13]~input_o\);

-- Location: IOIBUF_X28_Y29_N8
\bMKR_D[14]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMKR_D(14),
	o => \bMKR_D[14]~input_o\);

-- Location: IOIBUF_X28_Y0_N22
\bPEX_RST~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bPEX_RST,
	o => \bPEX_RST~input_o\);

-- Location: IOIBUF_X21_Y0_N29
\bPEX_PIN6~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bPEX_PIN6,
	o => \bPEX_PIN6~input_o\);

-- Location: IOIBUF_X16_Y0_N15
\bPEX_PIN8~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bPEX_PIN8,
	o => \bPEX_PIN8~input_o\);

-- Location: IOIBUF_X19_Y0_N1
\bPEX_PIN10~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bPEX_PIN10,
	o => \bPEX_PIN10~input_o\);

-- Location: IOIBUF_X19_Y0_N8
\bPEX_PIN12~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bPEX_PIN12,
	o => \bPEX_PIN12~input_o\);

-- Location: IOIBUF_X19_Y0_N29
\bPEX_PIN14~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bPEX_PIN14,
	o => \bPEX_PIN14~input_o\);

-- Location: IOIBUF_X35_Y0_N22
\bPEX_PIN16~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bPEX_PIN16,
	o => \bPEX_PIN16~input_o\);

-- Location: IOIBUF_X30_Y0_N15
\bPEX_PIN20~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bPEX_PIN20,
	o => \bPEX_PIN20~input_o\);

-- Location: IOIBUF_X30_Y0_N1
\bPEX_PIN28~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bPEX_PIN28,
	o => \bPEX_PIN28~input_o\);

-- Location: IOIBUF_X26_Y0_N1
\bPEX_PIN30~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bPEX_PIN30,
	o => \bPEX_PIN30~input_o\);

-- Location: IOIBUF_X41_Y18_N1
\bPEX_PIN32~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bPEX_PIN32,
	o => \bPEX_PIN32~input_o\);

-- Location: IOIBUF_X30_Y0_N8
\bPEX_PIN42~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bPEX_PIN42,
	o => \bPEX_PIN42~input_o\);

-- Location: IOIBUF_X37_Y0_N8
\bPEX_PIN44~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bPEX_PIN44,
	o => \bPEX_PIN44~input_o\);

-- Location: IOIBUF_X35_Y0_N1
\bPEX_PIN45~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bPEX_PIN45,
	o => \bPEX_PIN45~input_o\);

-- Location: IOIBUF_X37_Y0_N1
\bPEX_PIN46~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bPEX_PIN46,
	o => \bPEX_PIN46~input_o\);

-- Location: IOIBUF_X35_Y0_N8
\bPEX_PIN47~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bPEX_PIN47,
	o => \bPEX_PIN47~input_o\);

-- Location: IOIBUF_X41_Y23_N1
\bPEX_PIN48~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bPEX_PIN48,
	o => \bPEX_PIN48~input_o\);

-- Location: IOIBUF_X41_Y24_N8
\bPEX_PIN49~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bPEX_PIN49,
	o => \bPEX_PIN49~input_o\);

-- Location: IOIBUF_X41_Y24_N1
\bPEX_PIN51~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bPEX_PIN51,
	o => \bPEX_PIN51~input_o\);

-- Location: IOIBUF_X26_Y0_N8
\bWM_PIO1~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bWM_PIO1,
	o => \bWM_PIO1~input_o\);

-- Location: IOIBUF_X26_Y0_N29
\bWM_PIO2~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bWM_PIO2,
	o => \bWM_PIO2~input_o\);

-- Location: IOIBUF_X37_Y0_N29
\bWM_PIO3~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bWM_PIO3,
	o => \bWM_PIO3~input_o\);

-- Location: IOIBUF_X26_Y0_N15
\bWM_PIO4~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bWM_PIO4,
	o => \bWM_PIO4~input_o\);

-- Location: IOIBUF_X7_Y0_N15
\bWM_PIO5~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bWM_PIO5,
	o => \bWM_PIO5~input_o\);

-- Location: IOIBUF_X14_Y0_N22
\bWM_PIO7~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bWM_PIO7,
	o => \bWM_PIO7~input_o\);

-- Location: IOIBUF_X7_Y0_N22
\bWM_PIO8~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bWM_PIO8,
	o => \bWM_PIO8~input_o\);

-- Location: IOIBUF_X14_Y0_N1
\bWM_PIO18~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bWM_PIO18,
	o => \bWM_PIO18~input_o\);

-- Location: IOIBUF_X14_Y0_N8
\bWM_PIO20~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bWM_PIO20,
	o => \bWM_PIO20~input_o\);

-- Location: IOIBUF_X16_Y0_N29
\bWM_PIO21~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bWM_PIO21,
	o => \bWM_PIO21~input_o\);

-- Location: IOIBUF_X23_Y0_N8
\bWM_PIO27~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bWM_PIO27,
	o => \bWM_PIO27~input_o\);

-- Location: IOIBUF_X35_Y0_N15
\bWM_PIO28~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bWM_PIO28,
	o => \bWM_PIO28~input_o\);

-- Location: IOIBUF_X26_Y0_N22
\bWM_PIO29~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bWM_PIO29,
	o => \bWM_PIO29~input_o\);

-- Location: IOIBUF_X7_Y0_N29
\bWM_PIO31~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bWM_PIO31,
	o => \bWM_PIO31~input_o\);

-- Location: IOIBUF_X7_Y0_N8
\bWM_PIO34~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bWM_PIO34,
	o => \bWM_PIO34~input_o\);

-- Location: IOIBUF_X5_Y0_N1
\bWM_PIO35~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bWM_PIO35,
	o => \bWM_PIO35~input_o\);

-- Location: IOIBUF_X0_Y5_N15
\bWM_PIO36~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bWM_PIO36,
	o => \bWM_PIO36~input_o\);

-- Location: IOIBUF_X16_Y0_N22
\oWM_RX~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_oWM_RX,
	o => \oWM_RX~input_o\);

-- Location: IOIBUF_X0_Y4_N22
\oWM_RESET~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_oWM_RESET,
	o => \oWM_RESET~input_o\);

-- Location: IOIBUF_X0_Y4_N1
\bHDMI_SDA~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bHDMI_SDA,
	o => \bHDMI_SDA~input_o\);

-- Location: IOIBUF_X0_Y5_N22
\bHDMI_SCL~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bHDMI_SCL,
	o => \bHDMI_SCL~input_o\);

-- Location: IOIBUF_X0_Y3_N1
\bMIPI_SDA~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMIPI_SDA,
	o => \bMIPI_SDA~input_o\);

-- Location: IOIBUF_X0_Y3_N8
\bMIPI_SCL~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMIPI_SCL,
	o => \bMIPI_SCL~input_o\);

-- Location: IOIBUF_X14_Y0_N15
\bMIPI_GP[0]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMIPI_GP(0),
	o => \bMIPI_GP[0]~input_o\);

-- Location: IOIBUF_X30_Y0_N22
\bMIPI_GP[1]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMIPI_GP(1),
	o => \bMIPI_GP[1]~input_o\);

-- Location: IOIBUF_X0_Y25_N8
\oFLASH_MOSI~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_oFLASH_MOSI,
	o => \oFLASH_MOSI~input_o\);

-- Location: IOIBUF_X0_Y20_N22
\iFLASH_MISO~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_iFLASH_MISO,
	o => \iFLASH_MISO~input_o\);

-- Location: IOIBUF_X16_Y0_N8
\oFLASH_HOLD~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_oFLASH_HOLD,
	o => \oFLASH_HOLD~input_o\);

-- Location: IOIBUF_X16_Y0_N1
\oFLASH_WP~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_oFLASH_WP,
	o => \oFLASH_WP~input_o\);

-- Location: IOIBUF_X41_Y18_N22
\bMKR_D[6]~input\ : cyclone10lp_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_bMKR_D(6),
	o => \bMKR_D[6]~input_o\);
END structure;


