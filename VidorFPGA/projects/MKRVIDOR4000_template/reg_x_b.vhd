--------------------------------------------------------------
--! @file
--! @brief Registre modulable en taille destiné à être 
--! initialisé avec 80 bits.
--! @author Fontaine Pierre
--------------------------------------------------------------
library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;

--! @param clk horloge du système
--! @param init signal pour initialiser le registre avec le flux
--! d'entrée d'initialisation
--! @param rst signal pour un reset asynchrone
--! @param load signal pour autoriser l'écriture
--! @param x signal pour le flux d'entrée
--! @param x_init signal pour le flux d'entrée d'initialisation
--! @param x_out signal pour le flux de sortie
entity reg_x_b is
    generic (s : integer := 100);
    port (
        clk, init, rst, load    : in std_logic;
        x                       : in std_logic_vector(s -1 downto 0);
        x_init                  : in std_logic_vector(80 -1 downto 0);
        x_out                   : out std_logic_vector(s -1 downto 0)
    ) ;
end reg_x_b ; 

architecture reg_x_b_arch of reg_x_b is
    subtype mot is std_logic_vector(s-1 downto 0);
    type mem_array is array(1 downto 0) of mot;
    signal memory : mem_array;
begin

    main : process( clk, rst )
    begin
        if rst = '1' then
            memory(0) <= (Others => '0');
        elsif rising_edge(clk) then
            if load = '1' then
                if init = '1' then
                    memory(0) <= (s-1  downto 80 => '0') & x_init;
                else
                    memory(0) <= x;
                end if;
            end if;
        end if;
    end process ; -- main

    x_out <= memory(0);

end architecture ;